---
permalink: /about/
title: "About innercircles"
excerpt: "How we build circles."
sidebar:
  - image: /assets/images/site/sidebar-community-logo.png
    image_alt: "image"
    text: 'Learn more about our vision of a shared path. <b class="color-think">Let&#39;s dream!</b>'
  - nav: "nav-about"
---

In innercircles we think, act, feel, play and dream **the game of life™**. Our [**Mission**](mission/){: .color-act title="What we dream"} is to have you dream with us. Towards a humble [**Vision**](vision/){: .color-think title="What we realize"}: finding simple solutions. To prove they still exist.

---

<div class="page-menu color-think" markdown="1">
- [We are for people](#we-are-for-people){: .color-about}
- [We are for small and simple](#we-are-for-small-and-simple){: .color-about}
- [We build circles that expand](#we-build-circles-that-expand){: .color-about}
</div>

## We are for people

Helping others flourish will make us prosper ourself. We come together to cooperate in support of what makes us humans thrive. By revaluing humanity we'll unleash the freedom of humankind. In support of people in their daily lives. 

Our society has reached a stage of degradation. We move to the brink with increasing speed. Under the burden of an extractive system, driven by mindless consumerism and with individualism always on the rise. In this mealstrom we tend to forget the small and simple things in life. The things that will make us happier.

We are building pathways to a [**Flourishing Life**](../feel/flourish/){: .color-feel title="A practical approach to life that we develop cooperatively"} and make them accessible to everyone.

## We are for small and simple

We don't try to rally the masses. We are not out for fortune and fame. We are just for normal people. You and me. People who know that big is not always better. That infinite growth is impossible. That there are alternatives to endless scale. We provide an evolutionary lifestyle.

So many people are blinded by societal trends. Amassing wealth just for the sake of it. Wielding influence and power in its pursuit and to the detriment of others. In a rat-race cycle of devolution where ultimately no one wins.

Small is the exact opposite of that. Small is precious, small is beautiful. In fact it contains the solution. Actually we'll prove that [**Small is Huge**](../feel/flourish/philosophy/#small-is-huge){: .color-feel title="A philosophy to value and embrace smallness"}! And simple is the tool for that. Starting from the individual we simply go from there. To put our minds together and co-create the future. One that is sunny and bright.

## We build circles that expand

The game of life is a cooperative one. One best played with a close circle of friends and family and those dear to us. Society is formed when numerous such circles interact. Lasting change means reformulating the rules of this game so they allow us to play it better. This is one aspect of what innercircles does.

Starting small and simple we we focus on our own circles and make improvements, however tiny. Then we'll grow from there. We expand our radius, increase our reach, opening up and connecting circles and fan out further. Always connecting the dots.

As that happens we will broaden our horizons, embolden our vision and set higher goals. All that we build is meant to scale. Just small dreams of new reality that are infinitely scaleable. Bringing positive and lasting change is what innercircles is truly after.

Does this appeal to you? Open up your inner circles and come join us on this quest. Be a [**circle builder**](/play){: .color-play} with us!