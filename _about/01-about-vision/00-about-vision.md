---
permalink: /about/vision/
title: "Vision of innercircles"
excerpt: "We will prove that simple solutions still exist."
sidebar:
  - image: /assets/images/site/sidebar-community-logo.png
    image_alt: "image"
    text: 'Learn more about our vision of a shared path. <b class="color-think">Let&#39;s dream!</b>'
  - nav: "nav-about"
---

Our humble Vision consists of four _small_ parts that each represent one of the [Core Principles](../principles){: .color-feel} we hold dear and care about.

> **_"Come together, come fullcircle, grow, become whole."_**<br/>
> <i>— innercircles, vision</i>

This sentence respectively communicates our principles in visions of realistic [**mindfulness**](../principles#-mindfulness){: .color-think}, holistic [**sustainability**](../principles#-sustainability){: .color-act}, child-like [**playfulness**](../principles#-playfulness){: .color-play} and intrinsic [**happiness**](../principles#-happiness){: .color-feel}.

## Come together
{: .color-think}

Let's bring our minds together and think rational solutions, using common knowledge and our common sense. Participation and cooperation is what makes us strong.

## Come fullcircle
{: .color-act}

Let's live more in harmony with nature and our environment. Finding balance in what we extract and then give back. By downscaling our footprint and valuing the small fruits of life again.

## Grow
{: .color-play}

Let's unleash our human potential to the full extent. That what makes humanity great. Through education and exercise of our freedoms and human rights, so we can evolve again.

## Become whole
{: .color-feel}

Let's try to find happiness in all the things we do, and lead fulfilling and meaningful lives. By helping others, showing empathy and most importantly practice simple human values.
