---
permalink: /about/principles/
title: "Principles of innercircles"
excerpt: "Discover the core principles and intrinsic values that are dear to us."
sidebar:
  - image: /assets/images/site/sidebar-community-logo.png
    image_alt: "image"
    text: 'Learn more about our vision of a shared path. <b class="color-think">Let&#39;s dream!</b>'
  - nav: "nav-about"
---

These are the core principles and intrinsic values that are dear to us. The ones we will spread. We promote them actively, wherever we can. It is not hard, as they just come natural.

## Core principles

#### <span class="fas fa-brain"></span> Mindfulness
{: .color-think}

We strive to be in the present, in the here and now. Using our rational mind and critical thinking, combined with common wisdom, common knowledge, common sense and our collective life experience. Given our efforts, we accept what's coming for us. Mindfulness entails that:

- <span class="color-think">**Only progress is what counts.**</span> There exists no failure, and there is no success.
- <span class="color-think">**There is just what you provide.**</span> There are no obligations nor expectations.
- <span class="color-think">**There is only natural pace.**</span> There is no timescale, no pressure to perform.
- <span class="color-think">**Dreams guide to realization.**</span> Aspirations determine present-day opportunities.

#### <span class="fas fa-leaf"></span> Sustainability
{: .color-act}

Evolution means finding natural balance. For humans to flourish and humanity to thrive, we have to find the proper one. We are after an evolutionary lifestyle that enables that. It is a holistic approach. We explore the circles of sustainability and foster:

- <span class="color-act">**An inclusive an open culture.**</span> Creating a society where everyone fits in.
- <span class="color-act">**Healthy respect for our ecology.**</span> The environment is part of our natural habitat.
- <span class="color-act">**Vibrant and fair economy.**</span> One that provides equal access, equal chances.
- <span class="color-act">**Based on humane technology.**</span> With innovation that supports and sustains us.

#### <span class="fas fa-heart"></span> Happiness
{: .color-feel}

Everyone has the right to be happy. To lead a decent and fulfilling life. Happiness is an abstract concept. Partially a state of mind, influenced by the cards that life dealt us before, either good or bad. We create the conditions that allow us to be happier, by:

- <span class="color-feel">**Removing barriers to happiness.**</span> Removing blinders and dogma's that hold us back.
- <span class="color-feel">**Helping others to be happier too.**</span> As happiness flows in a two-way street.
- <span class="color-feel">**Supporting people that are in need.**</span> And learn how helping others helps yourself.
- <span class="color-feel">**Showing pathways to a happier life.**</span> Directions to follow and practices to adopt.

#### <span class="fas fa-theater-masks"></span> Playfulness
{: .color-play}

Look at how our children play. For them life has no boundaries. Everything is possible and dreams can come true. For children magic exists. They are innocent. As adults we forget all this playfulness, as life revolves around serious things. However, we intend to bring some of the magic back by:

- <span class="color-play">**Thinking out of the box.**</span> No obstructions. Everything is possible or we make it so.
- <span class="color-play">**Unleashing collective creativity.**</span> Stimulate the nourishment of new ideas.
- <span class="color-play">**Embracing exciting innovations.**</span> Adopt new uplifting technology to our benefit.
- <span class="color-play">**Enticing others to join the fun.**</span> Enjoy collective adventures, on a common quest.

## Intrinsic values

#### <span class="fas fa-route"></span> Freedom
{: .color-think}

Freedom of mind, freedom of spirit, freedom of choice and freedom of expression. These four freedoms are our human rights, and we value them deeply.

This is reflected first of all in our own organization. There is no real membership, you either participate whenever you so decide or you do not. What you do is entirely up to you, and we will not think any less of you for doing just that.

Besides that we promote these principles of freedom to everyone. They will be central in all our activities, the works we create, and the solutions we provide. We are out to free humanity.

#### <span class="fas fa-hands-helping"></span> Humanity
{: .color-act}

Treat others like we want to be treated yourself. Humanity is at its best, when human values and individual dignity are at the foreground of all the things we do.

Our modern society has mechanisms that make us forget these simple facts. It de-emphasizes many of the qualities that makes us decent human beings that can live along together in peace and harmony. Instead society seems to promote and atmosphere of distrust of each other's motives and intentions.

In innercircles we adhere to a trust-first approach in our relationships together. Engrained throughout our culture and overall approach, we will show that appreciation of true human values will lead us further than all else. 

