---
permalink: /about/mission/
title: "Mission of innercircles"
excerpt: "We are building solutions for human flourishing."
sidebar:
  - image: /assets/images/site/sidebar-community-logo.png
    image_alt: "image"
    text: 'Learn more about our vision of a shared path. <b class="color-think">Let&#39;s dream!</b>'
  - nav: "nav-about"
---

We build solutions for a better life, using the tools and knowledge that are readily available to any and all of us, but which we all too easily overlook.

> **_"Start with yourself, start small, activate others, grow. Just dream and play."_**<br/>
> <i>— innercircles, mission</i>

The wisdom and common sense to address our biggest problems is lying all around us. Mostly forgotten or trampled upon while we lead our hectic lives. We strive to bring obvious solutions within grasp, and make things simple and clear again. The only logical way to act.

## Start with yourself

We all have dreams and aspirations. Part of innercircles approach is to allow people to recognize what is important to them in life. To walk a path forwards towards leading lives that matter. We simply promote an evolutionary lifestyle that makes the good life easier to find.

Most solutions start with oneself. To take the right action and then stick to a path. In innercircles we make it real easy to make the first step, and then it is up to you if you want to follow along.

## Start small

Setting big goals in life is fine. By all means pursue grand visions if that's what you wish. But it sure helps to break these down into small and manageable pieces.

That is what we do to, and as we grow and fan out, there will be numerous fields where we operate. Find where you best fit in and participate. We provide a method where you'll not just help us, but yourself in the first place. Through mutualism and synergy we create [**people profit**](/feel/mutualism/){: .color-feel}.

We build services specifically targeted to strengthen [residential communities](/think/culture/residential-community/){: .color-think}, our circles where we spend most of our time. These include our closest friends and family, our next-door neighbors, the residents and businesses in our town or city, and also those we meet online.

## Activate others

We do not actively advertise ourself to the broader public. There is no need for PR and marketing campaigns to attract people to our cause. Our circle radius expands more naturally and based on a different activation strategy.

The people who count themselves circle builders just happened to find innercircles on their path. They might have met by pure chance. Or because an opportunity arose and they decided to act on it. In many cases though, they were introduced by a friend.

Becoming a circle builder involves an 'invite-to-participate' trust-first approach in welcoming newcomers. But that is not the only outreach that we do. The other activation mechanism is called "tease, nudge, play" and it is conducted by [teaserbot](/act/ecosystem/#-the-teaserbot-realm){: .color-play}.

## Grow

As innercircles grows, so should you do too. That is our objective and it is what will keep you active. Remember those dreams and life's objectives. You should pursue them where you can and want.

Get to know yourself better. Develop new skills, learn a new language. Meet more friends, socialize, and go on adventures. Find new interests, practice your hobby. Prepare for a career switch, your professional career or entrepreneurshihp.

Only you know best where the most value of innercircles resides. Participation leads to flourishment.

## Just dream and play

Most important of all is to stay yourself. Just be you, be relaxed and out for some fun and the pleasure. Reap the intrinsic rewards. We encourage you to follow your dreams, and to dream with us. While we go forward on an adventurous path. A path of magic. We'd love to see you come with us. Together we walk [The Path to the Inner Circle™](../feel/flourish/philosophy/#path-to-the-inner-circle){: .color-feel}.

And it is all part of [**The Game of Life**™](/feel/flourish/philosophy/#the-game-of-life){: .color-play}.