---
permalink: /act/
title: "Help evolve our circles"
excerpt: "This space presents our roadmap, plus status and details about our flagship projects."
sidebar:
  - image: /assets/images/site/sidebar-act.png
    image_alt: "fullcircle"
    text: 'Incubate, nourish, foster and grow sustainable circles. <b class="color-act">Let&#39;s act!</b>'
  - nav: "nav-act"
---

Welcome to the action! Find out how our flagship projects <span class="color-act">evolve</span> and join the <span class="color-play">development</span> of our ecosystem. Learn where you can jump in, and create some [**people profit**](../feel/mutualism/){: .color-feel title="Ways to benefit from innercircles"} here.

---

<div class="page-menu color-act" markdown="1">
- [Ongoing projects](#ongoing-projects)
- [The fullcircle ecosystem](#the-fullcircle-ecosystem)
- [Ecosystem building blocks](#ecosystem-building-blocks)
  - [groundwork](#-groundwork)
  - [solidground](#-solidground)
  - [outreach](#-outreach)
  - [innercircle center](#-innercircle-center)
  - [fullcircle today](#-fullcircle-today)
</div>

## Ongoing projects

We have has a range of interesting crowdsourced projects. Check them out and contribute. Here's our [**ongoing action**](ongoing/){: .color-act}.

## The fullcircle ecosystem

All our projects and deliverables become part of the <a href="ecosystem/" class="hover-underline"><b class="color-act">fullcircle</b> <span class="color-feel">ecosystem℠</span></a> The ecosystem is divided into four different **realms** which each has a public brand associated to them.

<div class="notice--act" markdown="1">
  <h3><span class="fas fa-leaf"></span> fullcircle ecosystem.. nourishment for circles</h3>
  Our tech projects are where insights come full circle, so we can scale **the game of life™**. Make it massively multi-player.
</div>

## Ecosystem building blocks

### <span class="fa fa-cubes"></span>&nbsp; groundwork
{: .color-act}

Groundwork is the federated server software that lays the **technology foundation** for all other innercircles automated services.

<table>
  <thead>
    <th width="50%">Project info</th>
    <th>Workspaces</th>
  </thead>
  <tbody>
    <tr>
      <td class="align-top">
        <ul>
          <li>project name: <a href="fullcircle-groundwork/" class="hover-underline"><b class="color-act">fullcircle</b> <span class="color-feel">groundwork™</span></a></li>
          <li>service: <b>n/a</b></li>
          <li>product site: <b>solidground.work</b></li>
          <li>project realm: <a href="ecosystem/#-the-fullcircle-realm" class="color-act"><b>fullcircle</b></a> </li>
          <li>status: <b><a href="fullcircle-groundwork/requirements/" class="color-act">minimum viable product</a></b></li>
        </ul>
      </td>
      <td class="align-top">
        <ul>
          <li>website: <b>tbd</b></li>
          <li>codebase: <b><a href="https://github.com/fullcircle-today/groundwork" class="color-think">github</a></b></li>
          <li>design space: <b><a href="https://codeberg.org/fullcircle/groundwork" class="color-think">codeberg</a></b></li>
          <li>documentation: <b>tbd</b></li>
          <li>discussion: <b><a href="https://discuss.innercircles.community/c/act/groundwork/29" class="color-think">forum</a></b></li>
          <li>chatroom: <b>tbd</b></li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

### <span class="fa fa-cubes"></span>&nbsp; outreach
{: .color-play}

Outreach is a grassroots **campaign management** system for the Fediverse, targeted at communities, non-profits, NGO's and sustainable businesses.

<table>
  <thead>
    <th width="50%">Project info</th>
    <th>Workspaces</th>
  </thead>
  <tbody>
    <tr>
      <td class="align-top">
        <ul>
          <li>project name: <a href="teaserbot-outreach/" class="hover-underline"><b class="color-play">teaserbot</b> <span class="color-think">outreach™</span></a></li>
          <li>service: <b class="color-play">teaserbot</b> <span class="color-think">outreach matters℠</span></li>
          <li>product site: <b><a href="https://getoutreach.org" class="color-play">getoutreach.org</a></b></li>
          <li>project realm: <a href="ecosystem/#-the-teaserbot-realm" class="color-play"><b>teaserbot</b></a> </li>
          <li>status: <b><a href="teaserbot-outreach/requirements/" class="color-think">elicit requirements</a></b></li>
        </ul>
      </td>
      <td class="align-top">
        <ul>
          <li>website: <b><a href="https://github.com/outreach-matters/getoutreach" class="color-think">github</a></b></li>
          <li>codebase: <b><a href="https://github.com/outreach-matters/outreach" class="color-think">github</a></b></li>
          <li>design space: <b><a href="https://codeberg.org/teaserbot-labs/outreach-matters" class="color-think">codeberg</a></b></li>
          <li>documentation: <b>tbd</b></li>
          <li>discussion: <b><a href="https://discuss.innercircles.community/c/act/outreach/33" class="color-think">forum</a></b></li>
          <li>chatroom: <b><a href="https://matrix.to/#/#outreachmatters:matrix.org" class="color-think">matrix</a></b></li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

### <span class="fa fa-cubes"></span>&nbsp; solidground
{: .color-act}

Solidground is a combined **collaboration** and **participation** platform. This set of Groundwork service modules allow people to interact in well-organized groups, called circles.

<table>
  <thead>
    <th width="50%">Project info</th>
    <th>Workspaces</th>
  </thead>
  <tbody>
    <tr>
      <td class="align-top">
        <ul>
          <li>project name: <a href="fullcircle-solidground/" class="hover-underline"><b class="color-act">fullcircle</b> <span class="color-feel">solidground™</span></a></li>
          <li>services:
            <ol>
              <li><b class="color-think">innercircles</b> <span class="color-feel">come together℠</span></li>
              <li><b class="color-feel">smallcircles</b> <span class="color-think">people profit℠</span></li>
              <li><b class="color-think">innercircles</b> <span class="color-feel">circles℠</span></li>
            </ol>
          </li>
          <li>product site: <b>solidground.work</b></li>
          <li>project realm: <a href="ecosystem/#-the-fullcircle-realm" class="color-act"><b>fullcircle</b></a> </li>
          <li>status: <b>ongoing research</b></li>
        </ul>
      </td>
      <td class="align-top">
        <ul>
          <li>website: <b>tbd</b></li>
          <li>codebase: <b>tbd</b></li>
          <li>design space: <b><a href="https://codeberg.org/fullcircle/solidground" class="color-think">codeberg</a></b></li>
          <li>documentation: <b>tbd</b></li>
          <li>discussion: <b><a href="https://discuss.innercircles.community/c/act/solidground/30" class="color-think">forum</a></b></li>
          <li>chatroom: <b>tbd</b></li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

### <span class="fa fa-cubes"></span>&nbsp; innercircle center
{: .color-think}

The innercircle center service modules provide the federated **control center** for fullcircle residential social networks. The centers are extensions of Solidground services.

<table>
  <thead>
    <th width="50%">Project info</th>
    <th>Workspaces</th>
  </thead>
  <tbody>
    <tr>
      <td class="align-top">
        <ul>
          <li>project name: <a href="innercircle-center/" class="hover-underline"><b class="color-think">innercircle</b> <span class="color-feel">center™</span></a></li>
          <li>service: <b class="color-act">fullcircle</b> <span class="color-feel">prepare for action℠</span></li>
          <li>product site: <b>innercircle.center</b></li>
          <li>project realm: <a href="ecosystem/#-the-innercircles-realm" class="color-think"><b>innercircles</b></a> </li>
          <li>status: <b>not started</b></li>
        </ul>
      </td>
      <td class="align-top">
        <ul>
          <li>website: <b>tbd</b></li>
          <li>codebase: <b>tbd</b></li>
          <li>design space: <b>tbd</b></li>
          <li>documentation: <b>tbd</b></li>
          <li>discussion: <b><a href="https://discuss.innercircles.community/c/act/innercircle-center/32" class="color-think">forum</a></b></li>
          <li>chatroom: <b>tbd</b></li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

### <span class="fa fa-cubes"></span>&nbsp; fullcircle today
{: .color-act}

The fullcircle today service modules and app constitute a **residential social network** that runs in support of a city, town or rural area, and federates with other full circles.

<table>
  <thead>
    <th width="50%">Project info</th>
    <th>Workspaces</th>
  </thead>
  <tbody>
    <tr>
      <td class="align-top">
        <ul>
          <li>project name: <a href="fullcircle-groundwork/" class="hover-underline"><b class="color-act">fullcircle</b> <span class="color-feel">today™</span></a></li>
          <li>service: <b class="color-act">fullcircle</b> <span class="color-feel">scan for action℠</span></li>
          <li>product site: <b>fullcircle.today</b></li>
          <li>project realm: <a href="ecosystem/#-the-fullcircle-realm" class="color-act"><b>fullcircle</b></a> </li>
          <li>status: <b>ongoing research</b></li>
        </ul>
      </td>
      <td class="align-top">
        <ul>
          <li>website: <b>tbd</b></li>
          <li>codebase: <b>tbd</b></li>
          <li>design space: <b>tbd</b></li>
          <li>documentation: <b>tbd</b></li>
          <li>discussion: <b><a href="https://discuss.innercircles.community/c/act/fullcircle-today/31" class="color-think">forum</a></b></li>
          <li>chatroom: <b>tbd</b></li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

