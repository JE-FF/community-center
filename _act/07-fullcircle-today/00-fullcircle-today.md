---
permalink: /act/fullcircle-today/
excerpt: "Summary and highlights of the fullcircle today special-purpose social network."
title: " "
seo_title: "Overview of fullcircle today™"
header:
  og_image: /assets/images/site/fullcircle-logo-square-white-ogimage.png
sidebar:
  - image: /assets/images/site/sidebar-act.png
    image_alt: "fullcircle"
    text: 'Incubate, nourish, foster and grow sustainable circles. <b class="color-act">Let&#39;s act!</b>'
  - nav: "nav-act"
---

<img src="{{ site.baseurl }}/assets/images/site/fullcircle-today-800x60.png"/>

The fullcircle today **residential social network** helps residents, local businesses and other organizations, as well as tourists nagivate a local community - a town, city or other geographic area - called a _fullcircle_. Users discover exciting actions, activities and events to join, and can easily enroll online. During their participation interactive features may be available to enhance their experience. Marketplace features may direct them to the interesting local shops and business, craftsmen and artisans, who can make them Offers online.

The software is free and open source (AGPLv3) and can connect to the [Fediverse](https://en.wikipedia.org/wiki/Fediverse). API's and plugin libraries allow great flexibility to extend the service with additional functionality.

---

<span class="color-act">• </span>[Ecosystem role](#ecosystem-role){: .color-act} <br/>
<span class="color-act">• </span>[Highlights](#highlights){: .color-act} <br/>
<span class="color-act">• </span>[Minimum viable product](#minimum-viable-product){: .color-act} <br/>

## Ecosystem role

Actions, offers, campaigns and other activities are prepared in an <a href="{{ site.baseurl}}/act/innercircle-center/" style="text-decoration: none"><b class="color-think">innercircle</b> <span class="color-feel">center™</span></a> - the supporting pillars of the social network - by creators called <b class="color-act">fullcircle</b> <span class="color-feel">insiders</span>. Once published to fullcircle today - the central access point for the broader public and part of the [fullcircle realm](../#-the-fullcircle-realm){: .color-act} - they can be consumed by anyone that is eligible to do so.

![Conceptual overview of fullcircle today]({{ "/assets/images/fullcircle-concepts-fullcircle-today.png" | relative_url }})

Many fullcircles can exist, and though they focus first on their own residents, they are also inter-connected with the other circles, sharing interactive content and other information.

### Scan for action

The main service of the fullcircle today is called <b class="color-act">fullcircle</b> <span class="color-feel">scan for action℠</span> and its purpose is - unlike traditional social networks - to encourage people to interact offline more often, to strengthen their real-world social relationships and stimulate the local economy.

The user experience is vibrant and refreshing and eliciting people's active engagement and creativity. The innercircles philosophy is interwoven in the network in a way that is entirely unobtrusive, but which brings the four innercircles themes into play:

1. **mindfulness**{: .color-think}: culture, museums, what is happening now, what is special, value intrinsic artifacts: e.g. paper before screens, live the day, find the unexplored, the unknown.
2. **sustainability**{: .color-act}: awareness of neighborhood and environment, attention to nature, small behavioral nudges, best practices, life hacks, promote engaging in sustainable activities.
1. **happiness**{: .color-feel}: elicit positive emotions and feelings, help overcome setbacks and negativity, stimulate social bonds, delightful user experience, bright designs, happy stories.
1. **playfulness**{: .color-play}: show where the fun is, turn dreary tasks into delicious games, quests, adventure, ethical gamification, involve artists and the arts, be more like children.

### Rewards

The same mechanism of [**people profit**](/feel/mutualism/){: .color-feel} that applies to all community circle builders, is also applied and deeply embedded in fullcircle today.

**Note**: There are other types of rewards too, but the entire reward system is a future point on the innercircles roadmap.
{: .notice--warning}

## Highlights

**Note**: Most of fullcircle today features are elaborated at a later stage. However, the federated servers of the network share an important part of core functionality with [innercircle center](../innercircle-center/) where code development will focus first.
{: .notice--warning}

Feature elicitation starts in parallel with <a href="{{ site.baseurl}}/act/innercircle-center/" class="hover-underline"><b class="color-think">innercircle</b> <span class="color-feel">center™</span></a> development. From a technical perspective this project will:

- Track which core functionality is common between projects and place them in shared libraries.
- Return feedback on any known requirements that affect the innercircle center development.

## Minimum viable product

The minimum viable product of this project is not yet defined, but its research and design is already part of the [ecosystem roadmap](../ecosystem/roadmap/#3b-research-and-design-fullcircle-today){: .color-act}.
