---
permalink: /act/teaserbot-outreach/requirements
title: "Outreach project requirements"
excerpt: "An overview of the requirements of the teaserbot outreach project."
sidebar:
  - image: /assets/images/site/sidebar-act.png
    image_alt: "fullcircle"
    text: 'Incubate, nourish, foster and grow sustainable circles. <b class="color-act">Let&#39;s act!</b>'
  - nav: "nav-act"
---