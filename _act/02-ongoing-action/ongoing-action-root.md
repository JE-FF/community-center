---
permalink: /act/ongoing/
title: "Our ongoing action"
excerpt: "The ongoing projects of innercircles that are under continual improvement."
sidebar:
  - image: /assets/images/site/sidebar-act.png
    image_alt: "fullcircle"
    text: 'Incubate, nourish, foster and grow sustainable circles. <b class="color-act">Let&#39;s act!</b>'
  - nav: "nav-act"
---

 The following ongoing projects evolve based on **continual improvement**. Visit the individual spaces, find where your interests lie, and how you can **contribute**.

---

<div class="page-menu color-act" markdown="1">
- [community center](#-community-center)
- [delightful project](#-delightful-project)
</div>

## <span class="fa fa-recycle"></span>&nbsp; community center
{: .color-feel}

The community center website (this site) has the following work areas:

- [**think**](/think/){: .color-think} **knowledgebase**: collecting ideas and knowledge to adopt in our projects.
- [**triple win**](/feel/triple-win/){: .color-feel} **directory**: tracking our efforts to maximize synergy of our collaboration.
- [**people-profit**](/feel/people-profit/){: .color-feel} **methodology**: collect practices to optimize mutualism in our relationships.
- [**flourishing life**](/feel/flourish/){: .color-feel} **methodology**: collect practices that allow people to thrive, be happier.
- [**evolutionary lifestyle**](/feel/flourish/lifestyle/){: .color-act} **philosophy**: investigating how to gradually bring harmony to life.
- [**gamification**](/play/tease/gamification/){: .color-play} **library**: ideate fun, educational and creative ways to spread our message.
- [**nudge**](/play/nudge/campaigns/){: .color-play} **campaigns**: promotional efforts to nudge people towards a better direction in life.

## <span class="fa fa-recycle"></span>&nbsp; delightful project
{: .color-play}

A collection of curated list of free and open software (FOSS), open data and open science resources. In innercircles we maintain lists for our [**think**](/think/){: .color-think} knowledgebase.

- <a href="https://codeberg.org/teaserbot-labs/delightful" class="color-think"><b class="color-play">delightful</b> gems of freedom</a>: top-level overview of all curated lists hosted on Codeberg.