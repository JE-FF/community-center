---
permalink: /act/innercircle-center/
excerpt: "Summary and highlights of the innercircle center communication and participation platform."
title: " "
seo_title: "Overview of innercircle center℠"
header:
  og_image: /assets/images/site/innercircle-center-ogimage.png
sidebar:
  - image: /assets/images/site/sidebar-act.png
    image_alt: "fullcircle"
    text: 'Incubate, nourish, foster and grow sustainable circles. <b class="color-act">Let&#39;s act!</b>'
  - nav: "nav-act"
---

<img src="{{ site.baseurl }}/assets/images/site/innercircle-center-800x60.png"/>

The innercircle center is a combined **collaboration** and **participation** platform. A center is server software that can be deployed stand-alone, or in a decentralized federation with other centers. The services it provides allow people to interact in well-organized groups, called circles, to help streamline processes in the real world. Circles are highly configurable. Depending on type, topic and configuration of a circle, various automated processes and activities help circle members with decision-making, governance tasks, meetings, preparation of events, actions and campaigns, as well as administration and control of external systems.

The software is free and open source (AGPLv3) and can be self-hosted independently of innercircles to be connected to the [Fediverse](https://en.wikipedia.org/wiki/Fediverse). The application is highly extensible via plugins, while the API allows easy integration with other software.

---

<span class="color-think">• </span>[Ecosystem role](#ecosystem-role){: .color-think} <br/>
<span class="color-think">• </span>[Highlights](#highlights){: .color-think} <br/>
<span class="color-think">• </span>[Minimum viable product](#minimum-viable-product){: .color-think}

## Ecosystem role

Within the [innercircles realm](../#-the-innercircles-realm){: title="Area of the fullcircle ecosystem"} the primary function of the innercircle centers is to be the supporting pillars of a <a href="{{ site.baseurl}}/act/fullcircle-today/" style="text-decoration: none"><b class="color-act">fullcircle</b> <span class="color-feel">today℠</span></a> residential social network. The actions and campaigns that are prepared on the center are launched and executed in the full circle, and results are returned back to the center.

![Conceptual overview of innercircle center]({{ "/assets/images/fullcircle-concepts-innercircle-center.png" | relative_url }})

### Prepare for action

The name of the service that the innercircle centers offer is called <b class="color-act">fullcircle</b> <span class="color-feel">prepare for action™</span> and it is available to all residents, small business owners, non-profits, schools, sports clubs and other organizations within the fullcircle. Circles participation can be public or require an Invitation, which can be accompanied with an Offer if they give access to paid services. The primary audience of innercircle center are the active people that drive the local economy and want to see it thrive.

## Highlights

Let's dive a bit into the features and characteristics of an innercircle center.

**Note**: The following features are merely _<u>indicative</u>_ still. The project will start by creating a [minimum viable product](#minimum-viable-product).
{: .notice--warning}

The application for end-users:

- A productive environment for focused cooperation
- Ability to plan and manage events, schedule meetings
- Have conversations in discussion threads and chat windows
- Hold votes and use other decision-making tools
- Prepare and review documentation
- Prepare actions and campaigns and trigger activities
- Easily navigate circles and find content and documentation
- Use action-oriented minimalistic task-based user interfaces
- Enjoy the distraction-less intuitive user experience

While administrators, moderators and developers can:

- Monitor the entire state of the system
- Moderate user activity, handle flags, block users
- Prepare for most use cases by configuration settings only
- Model Circle organization structures with ease
- Design activity types and screens with forms-based UI designer
- Use interactive widgets from a library or develop new ones
- Define processes with a simple workflow diagram editor
- Customize further using plugins, and many extension points
- Integrate with other systems using API's and libraries

## Minimum viable product

The MVP will allow the innercircles community to 'dogfood' their own product for internal communication between members and is planned on the [ecosystem roadmap](../ecosystem/roadmap/#dogfooding-innercircle-center).



