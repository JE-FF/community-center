---
permalink: /act/ecosystem/research/
title: "Ecosystem research"
excerpt: "Research and development info related to evolving our ecosystem."
sidebar:
  - image: /assets/images/site/sidebar-act.png
    image_alt: "fullcircle"
    text: 'Incubate, nourish, foster and grow sustainable circles. <b class="color-act">Let&#39;s act!</b>'
  - nav: "nav-act"
---

Tracking ongoing research and development topics related to [minimum viable product](../../ecosystem/roadmap#dogfooding-innercircle-center) development.

<div class="page-menu color-think" markdown="1">
- [System requirements evaluation](#system-requirements-evaluation){: .color-act}
- [Domain model exploration](#domain-model-elaboration){: .color-act}
- [DDD development process](#ddd-development-process){: .color-act}
</div>

## System requirements evaluation

**Info**: Non-functional requirements relevant when project grows in size and complexity (to be updated along the way).
{: .notice--info}

<details markdown="1"><summary>Overview of relevant non-functional requirements (click to expand).</summary>

<br/>

### Privacy

We value privacy to great extent. All applications are examplars of private-by-default design. Privacy-sensitive features are opt-in, and are clearly explained in Privacy Policy and Terms of Service.

#### GDPR compliance

The software will be implemented from the start to fully comply to the the EU's GDPR regulation.

### Security

The innercircle center project will adopt a security-first approach.

### Compatibility

Backend API's can be used by multiple clients, not necessarily developed in tandem with the backend, therefore there is a requirement to maintain backwards-compatibility and e.g. version restful interfaces.

### Extensibility

### Multi-tenancy

By default there is no multi-tenancy enabled. Implicitly then there is only one tenant, which consists of the federation of innercircle centers. But there are two additional levels of multi-tenancy that must be supported:

1. **Circle tenants** that form their own federation of circles and organizations.
2. **Organization tenants** that act within a federation that may or may not be provided by a circle tenant.

### Accessibility

### Internationalization

And localization.

### Service level

Availability, resilience, robustness, durability, reliability, fault tolerance, operability, serviceability.

### Deployment

Installation, upgrades.

### Discoverability

[Discoverability](https://en.wikipedia.org/wiki/Discoverability) is the degree to which of something, especially a piece of content or information, can be found. This requirement also encompasses [findability](https://en.wikipedia.org/wiki/Findability).

### Licensing

The licensing of content that is created for the services we offer is dependent on the license chosen by the _Owner_ of the server. By default, and chosen as our standard, the license for public content is [Creative Commons Attribution-ShareAlike 3.0 Unported License](https://creativecommons.org/licenses/by-sa/3.0/) ("CC-BY-SA").

**Note**: The licensing must be explained in a Terms of Service. For innercircles we will adopt best-practices from [Wikipedia's licensing model](https://meta.wikimedia.org/wiki/Terms_of_use#7._Licensing_of_Content) which specifies how Copyright and Attribution works (dividing contributors in Authors and Editors).
{: .notice--info}

**Note**: Need to determine how to handle licensing for content that is not public.
{: .notice--warning}

</details>

### Domain model elaboration

**Info**: Brainstorm of possible future domain model extensions. Focused on automating patterns adopted from [Sociocracy](https://patterns.sociocracy30.org/map.html){: title="A social technology for evolving agile and resilient organizations" }.
{: .notice--info }

<details markdown="1"><summary>Circle cooperation domain model (click to expand).</summary>

<br/>

The following domain diagram sketches how the Solidground _might_ be set up to include a wide range of cooperation facilities:

![Coordination domain model]({{ "/assets/images/fullcircle-solidground-coordination-domain-concept.svg" | relative_url }})

### Circles

- Circle members are _Actors_. An actor can be:
  - A person that is a **Participant**.
  - An **Organization** of any kind.
  - Another **Circle**.
  - A **Bot** or automated script.
  - An **Interface** to another system.
- Circles can be:
  - **Nested**, creating a hierarchy.
  - **Related**, creating a network.
  - **Protected** by access restrictions.
  - **Governed** by one or more policies.
  - **Process-driven** by a workflow.
  - **Ephemeral**, disbanded once work is done.
  - **Federated**, hosted on multiple servers.
- Circle activities and communication can be:
  - **Topical** or domain-specific.
  - **Delegated** to another Circle.
  - **Moderated** by designated actors.
  - **Archived** for retention.
  - **Encrypted** where confidentiality is key.
  - **Processed** by a Bot or a Interface actor.

### Actors

- Participant actors can have an identity that is:
  - **Anonymous** requiring no personal information.
  - **Pseudonymous** requiring a user name and registration.
  - **Validated** requiring verification by authoritative actors.
- Participants can log in with their Fediverse account.

</details>

## DDD development process

**Info**: Exercise to see how the [MVP domain model](../requirements#domain-model) fans out into individual [BDD Use Cases](https://en.wikipedia.org/wiki/Behavior-driven_development) when using using DDD. 
{: .notice--info }

<details markdown="1"><summary>Exercise: From model to use cases (click to expand).</summary>

<br/>

<div class="page-menu color-think" markdown="1">
- [Stakeholders](#stakeholders)
- [Domain models](#domain-models)
  - [Circles domain](#circles-core-domain)
    - [Membership subdomain](#membership-core-subdomain)
    - [Documentation subdomain](#documentation-core-subdomain)
    - [Discussion subdomain](#discussion-core-subdomain)
    - [Reactions subdomain](#reactions-core-subdomain)
    - [Moderation subdomain](#moderation-supporting-subdomain)
    - [Federation subdomain](#federation-supporting-subdomain)
  - [User domain](#user-generic-domain)
    - [Admin domain](#admin-generic-subdomain)
    - [Reporting domain](#reporting-generic-subdomain)
    - [Notification domain](#notification-generic-subdomain)
    - [Development domain](#development-generic-subdomain)
- [Non-functional requirements](#non-functional-requirements)
  - [Privacy](#privacy)
  - [Security](#security)
  - [Accessibility](#accessibility)
  - [Internationalization](#internationalization)
  - [Extensibility](#extensibility)
  - [Deployment](#deployment)
  - [Service level](#service-level)
  - [Discoverability](#discoverability)
  - [Licensing](#licensing)
</div>

### Stakeholders

Based on the [domain models](#domain-models) we recognize the following set of primary stakeholders that are part of the core domain:

| Stakeholder | Description |
| :--- | :--- |
| **Participant** | A person that cooperates with others in one or more circles. |
| **Circle** | A group of participants working together in the groups interest. |
| **Actor** | Either a Participant or a Circle of the federated service. |
| **Follower** | A user that follows a circle to be informed about its activity. |
| **Member** | A user that joined a circle to actively participate with it. |
| **Creator** | A member in relation to a document or a comment they created. |

Additionally there are a number of supporting stakeholder:

| Stakeholder | Description |
| :--- | :--- |
| **Public** | Any person that accesses innercircle center without signing in. |
| **User** | A person that is signed in to an innercircle center server. |
| **Admin** | A user with full access rights to administer the server. |
| **Owner** | A provider that pays for the server that hosts innercircle center. |
| **Builder** | A person that extends and maintains the system software. |
| **Service** | An aggregated service formed by the server systems in a federation. |
| **System** | A server that hosts (parts of) the innercircle center services. |

**Note**: Modeling of a _Service_ is not part of the minimum viable product. There is implicitly only a single service.
{: .notice--info }

### Domain models

#### Circles (core domain)

Top-level domain provided by innercircles center with use cases for _User_, _Follower_, _Public_ and _Admin_ stakeholders:

| Ref. | Stakeholder | Description |
| :---: | :---: | :--- |
| _CRL-01_{: #crl-01} | Admin<sup>*</sup> | **Create circle**: Creates a new circle with a name, a title and optional description and hashtags (i.e. topics). The name is used to reference the circle in mentions, while the title is the circle's display name. |
| _CRL-02_{: #crl-02} | Admin<sup>*</sup> | **Define circle interest**: Attaches a document to the circle that describes its purpose, and other relevant information. The document has a free format. |
| _CRL-03_{: #crl-03} | Public | **View circle details**: Displays the circle name, its description and interest if defined. Also it displays metadata about the circle, such as number of members, documents, discussions as well as which other circles the circle actor participates in. The view does not include the circle members list. |
| _CRL-04_{: #crl-04} | Public | **View public circle activity**: Displays the paged list of circle documents that have been created, their creation date and the number of comments made while discussing it. By default the sort order is newest first. |
| _CRL-05_{: #crl-05} | Participant | **Follow circle activity**: By subscribing as a follower of a circle, as participant receives notifications about the public activity that is published from that circle. The circle activity received by followers may be more than the circle activity that is publicly viewable. |
| _CRL-06_{: #crl-06} | Follower | **Unfollow circle activity**: A follower can unfollow a circle to stop receiving the published stream of circle activity from it. |
| _CRL-07_{: #crl-07} | Admin<sup>*</sup> | **Lock circle**: Locking a circle means that no activity within that circle can take place anymore. It stays in the state it was at the time when it was locked. The circle still provides read-only access as before. |
| _CRL-08_{: #crl-08} | Admin<sup>*</sup> | **Unlock circle**: When a circle is in a locked state, unlocking it brings it back to active state and normal operation is possible again. |
| _CRL-09_{: #crl-09} | Admin<sup>*</sup> | **Delete circle**: Removes the circle from the system including all the documents and discussions that are related to it. |

<b>\*</b> For the minimum viable product only the Admin stakeholder can perform these use cases.
{: .notice--info}

#### Membership (core subdomain)

This core subdomain deals with joining a circle plus defining and adopting roles, involving _User_, _Member_ and _Admin_ stakeholders.

| Ref. | Stakeholder | Description |
| :---: | :---: | :--- |
| _MBS-01_{: #mbs-01} | Participant | **Join circle**: Adds the participant to the members of the circle. This direct joining of the membership is the default for public circles. Upon inclusion to the circle the particpant receives a notification which also includes a link to leave the group again. The participant, now a member, is assigned the default role of Participant (this role is not visible, it is implied). |
| _MBS-02_{: #mbs-02} | Member | **Leave circle**: At any time a member can decide to leave a circle. When leaving optionally they can include a reason for why they leave. A member leaving also automatically unassigns them from any roles they participated in. |
| _MBS-03_{: #mbs-03} | Admin<sup>*</sup> | **Define circle role**: Creates a role with a title, description, a default handle and an optional document to further describe the role's responsibilities. The role can be either circle-specific or globally defined for the entire system. The handle is used for mentions in documents and comments and will lead to notification of the members in that role in the circle where they were mentioned.
| _MBS-04_{: #mbs-04} | Admin<sup>*</sup> | **Manage circle roles**: Allows adding or removing of one or more roles to a particular circle. The available roles in turn they can be assigned to members within that circle. If a role is removed this affects future activities within the circle, but past activities performed in that role are unaffected (the role is recorded with the activity). |
| _MBS-05_{: #mbs-05} | Admin<sup>*</sup> | **Assign member role**: Allows a member to participate in the specified role. When the role is mentioned in group activity the member is notified in similar ways as if they were mentioned directly by username. Upon assignment the member receives a notification with the information about the role, so they know what is expected of them. |
| _MBS-06_{: #mbs-06} | Member | **Leave role**: At any time a member can decide to leave a role. When leaving they must include a reason for why they leave. They can choose from default reasons and add additional text. Other group members are informed with a notification about the membership change. |
| _MBS-07_{: #mbs-07} | Admin<sup>*</sup> | **Unassign member roles**: Removes the specified member from one or more roles. This use case differs from leaving a role, because some other stakeholder does the removal. A reason for the removal must be provided. Other group members are informed with a notification about the membership change. |
| _MBS-08_{: #mbs-08} | Member | **Invite members**: Members of a circle can invite one or more other users to become a member to. Each of the users that is invited receives a notification with a link to join the circle. |
| _MBS-09_{: #mbs-09} | Admin<sup>*</sup> | **Delete role**: When a role is deleted an entry is added to the activity stream of each circle that has the role. All members in that role are unassigned from it and receive a notification about the unassignment. |
| _MBS-10_{: #mbs-10} | Admin<sup>*</sup> | **Remove member**: Removing a member from a circle means they can no longer participate in the circle. A reason for the removal must be provided. The removal is shown in the circle's activity. All circle members including the now removed member receive a notification. |

<b>\*</b> For the minimum viable product only the Admin stakeholder can perform these use cases.
{: .notice--info}

#### Documentation (core subdomain)

Documentation is one of the primary processes of innercircle center and involves _Public_, _Member_, _Creator_ and _Admin_ stakeholders.

| Ref. | Stakeholder | Description |
| :---: | :---: | :--- |
| _DOC-01_{: #doc-01} | Member | **Create document**: Creates a new document within a circle. The document must have a title, while a description and set of topics (hashtags) are optional. Upon creation the document the 'created' and 'creator' metadata are set (the member is the creator. See also: [Licensing](#licensing)). It is possible to create empty documents, i.e. saving without content. |
| _DOC-02_{: #doc-02} | Public | **View document**: Shows the formatted document in read-only mode. By default all documents are public. |
| _DOC-03_{: #doc-03} | Member | **Edit document**: Shows the edit window for a new or existing document. The document is now in edit mode, and all its properties and content can be modified. Note, this does not include metadata. The editing format is [markdown](https://en.wikipedia.org/wiki/Markdown), which can be either typed directly or formatting is done via a toolbar. A member can only edit the documents they created. |
| _DOC-04_{: #doc-04} | Member | **Save document**: Stores the document that is in edit mode in its current state, after which the member can continue editing. The 'updated' and 'editor' metadata fields are updated accordingly (on first save the member is now also the editor. See also: [Licensing](#licensing)). |
| _DOC-05_{: #doc-05} | Creator | **Unlist document**: When a creator deletes their document, it becomes unlisted. The document still exists and so do the reactions and discussion thread that may be attached to it, but it is now only accessible to the admin. Note that after a member leaves a circle they are still the creator of their documents and are able to unlist them. |
| _DOC-06_{: #doc-06} | Admin<sup>*</sup> | **Delete document**: Removes the document from the system, including any reactions and discussion thread if they exist. |

<b>\*</b> For the minimum viable product only the Admin stakeholder can perform these use cases.
{: .notice--info}

#### Discussion (core subdomain)

Next to documentation, having discussions is another primary process of innercircle center, and it involves _Public_, _Member_, _Creator_ and _Admin_ stakeholders.

| Ref. | Stakeholder | Description |
| :---: | :---: | :--- |
| _DCS-01_{: #dcs-01} | Member | **Start discussion**: Creates a new discussion thread within a circle. A discussion can refer to a subject, which is usually a document. When no subject was provided there is no subject is provided it the member must specify a title and an optional set of topics (hashtags). |
| _DCS-02_{: #dcs-02} | Member | **Add comment**: Shows an edit window where the member can type their comment in markdown syntax (similar to [Edit document](#doc-03)). The comment can be the top-level comment of the discussion thread or created in response to another comment. Empty comments are not allowed. After adding the 'created' and 'creator' metadata are set (the member is now the creator. See also: [Licensing](#licensing)). |
| _DCS-03_{: #dcs-03} | Creator | **Edit comment**: Shows the edit window and allows the creator to update the comment. Editing a comment is only possible as long as there were no direct responses to it (i.e. it is last in its discussion tree branch).
| _DCS-04_{: #dcs-04} | Public | **View discussion timeline**: Shows the discussion thread as a flat list. The default order of the comments is by creation date. |
| _DCS-05_{: #dcs-05} | Public | **Sort discussion timeline**: Sorts the discussion thread by creation date, either newest comment first, or oldest comment first. |
| _DCS-06_{: #dcs-06} | Public | **View discussion tree**: Displays the discussion thread as a hierarchy, with the first comment displayed a the top and other comments sorted by creation date. |
| _DCS-07_{: #dcs-07} | Creator | **Unlist comment**: When a creator deletes a comment which has responses from other members, then the comment is unlisted. It becomes invisible to anyone but the administrator. In its place a placeholder is displayed informing others that a comment was deleted. |
| _DCS-08_{: #dcs-08} | Creator | **Delete comment**: A comment can be totally removed from the system if it does not have any responses. In this case no placeholder is displayed. |
| _DCS-09_{: #dcs-09} | Admin<sup>*</sup> | **Lock discussion**: After a discussion is locked no new comments or reactions can be added, but it is still possible for creators to unlist or delete comments. |
| _DCS-10_{: #dcs-10} | Admin<sup>*</sup> | **Unlock discussion**: Unlocking a locked discussion opens the discussion thread again, and both new comments and reactions can be added. |
| _DCS-11_{: #dcs-11} | Admin<sup>*</sup> | **Unlist discussion**: Hides the entire discussion thread, so that only the admin can still access it. |
| _DCS-12_{: #dcs-12} | Admin<sup>*</sup> | **Delete discussion**: Physically deletes the entire discussion thread from the system. |

<b>\*</b> For the minimum viable product only the Admin stakeholder can perform these use cases.
{: .notice--info}

#### Reactions (core subdomain)

| Ref. | Stakeholder | Description |
| :---: | :---: | :--- |
| _RCT-01_{: #rct-01} | Participant | **Add reaction**: Certain objects, such as documents and comments, allow users to give their reaction to it. Multiple reaction types are possible, but the most prominent one is the Like (or Upvote). A reaction of a certain type can only be given once on a specific object. |
| _RCT-02_{: #rct-02} | Participant | **Remove reaction**: Removes a reaction that a participant gave to an object. |
| _RCT-03_{: #rct-03} | Public | **View reactions**: Shows an aggregated count of all reactions given to an object. |
| _RCT-04_{: #rct-04} | Member | **View reaction details**: Shows the details of all reactions given to an object. The details are comprised of the users that gave the reaction and whether they are a member of the circle or not. |
| _RCT-04_{: #rct-04} | Admin<sup>*</sup> | **Unlist reactions**: Hides the reactions on an object, like a document or comment, or for an entire discussion thread. |

<b>\*</b> For the minimum viable product only the Admin stakeholder can perform these use cases.
{: .notice--info}

**Note**: Reactions play an important role in the cooperation processes. They must be carefully crafted using humane design principles. The use cases above represent the bare minimum. Currently there are no notifications about reactions that were given.
{: .notice--warning}

#### Moderation (supporting subdomain)

| Ref. | Stakeholder | Description |
| :---: | :---: | :--- |
| _MOD-01_{: #mod-01} | User | **Report user**: Any user has the ability to report bad behavior of another user to the admin. The user can choose from a number of reasons for the report, but must also specify an additional motivation. After creating the report the admin is notified with a moderation request. The reported user is not notified. |
| _MOD-02_{: #mod-02} | User | **Flag object**: Any user has the ability to flag object, like documents and comments. The user can choose from a number of reasons for the flag, but must also specify an additional motivation. After creating the flag the admin is notified with a moderation request. Depending on the reason the creator may be notified directly via a warning. |
| _MOD-03_{: #mod-03} | Admin<sup>*</sup> | **View moderation requests**: Shows a paged list of all open and handled moderation requests. |
| _MOD-04_{: #mod-04} | Admin<sup>*</sup> | **Filter moderation requests**: The list of moderation requests can be filtered by circle and by user. |
| _MOD-05_{: #mod-05} | Admin<sup>*</sup> | **Handle moderation request**: Shows the details of the moderation request, and the actions that the admin can take to solve the moderation issue. The admin can open a discussion thread to the originator of the request, can unlist or delete the flagged object, warn a user, remove a member from a circle, or terminate their user account. When the request is handled it is either accepted or rejected (no action taken). |
| _MOD-06_{: #mod-06} | Admin<sup>*</sup> | **Access unlisted object**: If an object, such as a document, discussion thread or individual comment was unlisted by a user, then the admin can still access and interact with it. Unlisted objects are displayed in the locations where they exist. |
| _MOD-07_{: #mod-07} | Admin<sup>*</sup> | **Restore unlisted object**: In most cases an admin can restore an unlisted object, so that for other users it appears to be 'undeleted' and they can interact with it as normal. |
| _MOD-08_{: #mod-08} | Admin<sup>*</sup> | **Warn user**: A warning is created for a user for which a moderation request was accepted. The admin can also send warnings unrelated to a prior moderation request. Upon doing so the warning is packaged with a moderation request. Optionally the admin can start a discussion thread to the targeted user. |
| _MOD-09_{: #mod-09} | User | **View warnings**: A user can see the list of warnings they received and view their details. If the warning relates to a moderation request that has not yet been handled, then the user may be able to interact with it (e.g. discuss with the admin). Once handled interaction is no longer possible. |
| _MOD-10_{: #mod-10} | Admin<sup>*</sup> | **Block access**: Adds a user to a blocklist that is attached to an object. Most commonly that of a circle, so that they cannot join the circle. If they were a member of the circle their membership is removed. Removing the entry from the blocklist restores the access, but not the membership. |

<b>\*</b> For the minimum viable product only the Admin stakeholder can perform these use cases.
{: .notice--info}

#### Federation (supporting subdomain)

| Ref. | Stakeholder | Description |
| :---: | :---: | :--- |
| _FED-01_{: #fed-01} | Participant | **Federated activities**: Allows participants on one instance to participate in the circles of another instance. |
| _FED-02_{: #fed-02} | Circle | **Federated circles**: Allows circles to join or follow other circles defined on federated server instances, and participants to interact with them seamlessly. |
| _FED-03_{: #fed-03} | Participant | **Fediverse integration**: Allows participants to interact with the Fediverse, specifically with microblogging applications such as Mastodon and Pleroma. |

#### User (generic domain)

Standard, not core to the service this subdomain has use cases for _User_ stakeholders:

| Ref. | Stakeholder | Use case and description |
| :---: | :---: | :--- |
| _USR-01_{: #usr-01}  | Public | **Register new user**: To become a user a person must register their username, password and a valid email address. The username cannot be changed after registration. |
| _USR-02_{: #usr-02} | Public | **Send email verification**: After signing up the system sends an email notification with a link to confirm and verify the email. |
| _USR-03_{: #usr-02} | Public | **Verify email address**: Clicking the link in the email verification message triggers the verification on the server. If the verification succeeds, then this is recorded and the user is valid. Only with a verified email address the user is allowed access to the system. |
| _USR-04_{: #usr-03} | Public | **Sign in user**: To gain access to the innercircle center services a user signs in with a valid username and password. |
| _USR-05_{: #usr-04} | User | **Sign out user**: A user that is signed in can sign out at any time. This action will sign them out from all devices they were signed in with. |
| _USR-06_{: #usr-05} | User | **Reset password**: If the user forgot their password they can request a password reset and receive an email notification with a link to a password reset screen where they can set a new password. A successful reset allows them to subsequently log in with that password. |
| _USR-07_{: #usr-06} | User | **View private profile**: A user can view their own user profile which shows all the user details of the user including email address and other personal information. |
| _USR-08_{: #usr-07} | Public | **View public profile**: Anyone can view the public profile of another user, which displays a subset of the user details of that user's private profile. |
| _USR-09_{: #usr-08} | User | **Edit user profile**: A user can edit the user details of their private profile and save the changes. Only their username cannot be changed after registration. Changing the password is a separate use case. |
| _USR-10_{: #usr-09} | User | **Change password**: When signed in a user can change their password by typing the old password, followed by the new one twice to ensure it is typed correctly. |
| _USR-11_{: #usr-10} | User | **Close user account**: A user can terminate their own account . When doing this they are provided they are provided the option to have all their activities to be either removed or anonymized (depending on whether it is private or public data). Then their personal profile is deleted, after which the user is automatically signed out of the system. |
| _USR-12_{: #usr-11} | Public | **Request authentication**: When actions are requested that require authentication, then a redirection is made to the login and signup facility. After authentication is established the user is directed back to the page where the request was made. |

#### Admin (generic subdomain)

| Ref. | Stakeholder | Description |
| :---: | :---: | :--- |
| _ADM-01_{: #adm-01} | Admin | **Terminate user account**: An admin can terminate a user's account . Upon termination all their activities to be either removed or anonymized (depending on whether it is private or public data). Then their personal profile is deleted, after which the user is automatically signed out of the system. The admin must provide a reason for the termination. All circles where the user was a member are notified that the user no longer exists (but no reason is provided). |
| _ADM-02_{: #adm-02} | Admin | **Block IP address**: Blocks an IP address from access to the server. |

#### Reporting (generic subdomain)

| Ref. | Stakeholder | Description |
| :---: | :---: | :--- |
| _RPT-01_{: #rpt-01} | User | **View dashboard**: Provides an overview of all places where the user is active and easy navigation to these places. |
| _RPT-02_{: #rpt-02} | User | **View activity log**: Shows a paged list of activities that can be filtered per circle or per user, and sorted by column in regular or reverse order. The activities are only summaries that include a link to the full activity in the context where they were created. |

#### Notification (generic subdomain)

| Ref. | Stakeholder | Use case and description |
| :---: | :---: | :--- |
| _NOT-01_{: #not-01} | User | **Specify notification settings**: A user can specify in which way and when they receive notifications from the system. |
| _NOT-02_{: #not-02} | System | **Notify user**: Sends a notification to a user about service or system activity and events. Email notification is a required notification channel for all users. Other channels are optional and are invoked based on the user's notification settings. |

#### Development (generic subdomain)

| Ref. | Stakeholder | Use case and description |
| :---: | :---: | :--- |
| _DEV-01_{: #dev-01} | User | **Provide feedback**: From any location a user can provide feedback for the development team (builders). Apart from the provided feedback additional information is sent along, such as page information, client information, etc. |

</details>