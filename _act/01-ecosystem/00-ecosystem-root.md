---
permalink: /act/ecosystem/
title: "Our ecosystem"
excerpt: "A helicopter view of the innercircles community landscape."
sidebar:
  - image: /assets/images/site/sidebar-act.png
    image_alt: "fullcircle"
    text: 'Incubate, nourish, foster and grow sustainable circles. <b class="color-act">Let&#39;s act!</b>'
  - nav: "nav-act"
---

The innercircles projects and other deliverables are all part of the <b class="color-act">fullcircle</b> <span class="color-feel">ecosystem℠</span>, which is divided into four main _realms_ - we avoid the word 'brands' here.

<div align="center">
  <img src="{{ site.baseurl}}/assets/images/fullcircle-ecosystem-realms.png" alt="The fullcircle ecosystem"/>
</div>

The four areas match innercircles [**Core principles**](../about/principles/){: .color-feel} and closely align to [**Mission**](../about/mission/){: .color-act} and [**Vision**](../about/vision){: .color-think} one-liners. Lastly they reflect our process contained in the credo _[**"think,**](../../think/){: .color-think} [**act,**](../){: .color-act} [**feel,**](../../feel/){: .color-feel} [**play"**](../../play/){: .color-play}_.

## <span class="fa fa-brain"></span>&nbsp; The innercircles realm
{: .color-think}

Projects and solutions developed here are targeted to facilitate ever more streamlined and inter-connected communication, not only for community participants but for anyone using the innercircles services. Also part of the deliverables are the documentation and content related to culture and philosophy concepts. The flagship - most important - project being worked on in this realm is <a href="{{ site.baseurl}}/act/innercircle-center/"  class="hover-underline"><b class="color-think">innercircle</b> <span class="color-feel">center℠</span></a>. While these centers can be deployed independently by anyone, within innercircles they are serving as supporting pillars of the fullcircle realm.

<div class="notice--think" markdown="1">
  <h3><span class="fas fa-brain"></span> Come together.. think, mind, air</h3>
  The innercircles realm will bring people together to socialize and **think**, air their differences and similarities in mindful ways.
</div>

## <span class="fa fa-leaf"></span>&nbsp; The fullcircle realm
{: .color-act}

This realm is targeting the outside world, the broader public - the 'end-users' - of innercircles. Their quest of discovery starts when they begin to participate and interact with our primary service, the <a href="{{ site.baseurl}}/act/fullcircle-today/" class="hover-underline"><b class="color-act">fullcircle</b> <span class="color-feel">today℠</span></a> residential social network, which is the single most important project that will be created by our community. Unlike most social networks, fullcircle today facilitates only relatively small groups of users that are part of a specific geopgraphic area, like a town or city or small rural area. But there can be many such communities, many fullcircles, which can be inter-connected by federated innercircle centers.

<div class="notice--act" markdown="1">
  <h3><span class="fas fa-leaf"></span> Come fullcircle.. act, earth, body</h3>
  The fullcircle realm allows people freedom to **act** naturally in harmony with our earth, and in ways that improve the body.
</div>

## <span class="fa fa-theater-masks"></span>&nbsp; The teaserbot realm
{: .color-play}

This is a special realm that will be gradually revealed. But it has a purpose to bring playfulness in all we do, and learning people how to play. At this point teaserbot is the entity that gently guides people to our community and takes them on a tour of discovery. Themes in this realm are about learning and education. Objectives are growth, both at personal level and in numbers.

<div class="notice--play" markdown="1">
  <h3><span class="fas fa-theater-masks"></span> Grow.. tease, nudge, play</h3>
  The teaserbot realm provides gentle teases and helpful nudges to help people get further in life. It helps us to **play**.
</div>

## <span class="fa fa-hands-helping"></span>&nbsp; The smallcircles realm
{: .color-feel}

A small circle refers to the individual. How a person is rooted in life and how they can flourish. This realm is about finding ways to more happiness, even when life throws you bad cards. The services in this realm start with the basic support that people need in life, which includes a decent monetary income. But more importantly this realm puts human value on the balance sheet, and will strive to make people rich in this regard.

<div class="notice--feel" markdown="1">
  <h3><span class="fas fa-hands-helping"></span> Become whole.. feel, ether, soul</h3>
  The smallcircles realm deals with the emotions that make us human, spiritial wellbeing of the soul. It allows us to **feel**.
</div>

## The game of life

All four realms are brought together in the **game of life™**. Together we walk our path on a quest to shape the landscape so it matches our dreams: innercircles then is _mindcraft for reality_.

> **_Beauty of mind creates freedom. Freedom of mind creates beauty._**
> _− Ann Demeulemeester, Belgian fashion designer_