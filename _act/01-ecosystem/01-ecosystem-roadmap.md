---
permalink: /act/ecosystem/roadmap/
title: "Ecosystem roadmap"
excerpt: "An overview of planned project development for the fullcircle ecosystem."
sidebar:
  - image: /assets/images/site/sidebar-act.png
    image_alt: "fullcircle"
    text: 'Incubate, nourish, foster and grow sustainable circles. <b class="color-act">Let&#39;s act!</b>'
  - nav: "nav-act"
---

Our roadmap for the <a href="{{ site.baseurl}}/act/ecosystem/" class="hover-underline"><b class="color-act">fullcircle</b> <span class="color-feel">ecosystem™</span></a> highlights the top-level activities that are important to the evolution of innercircles. The roadmap items are organized by priority in decreasing order.

---

<div class="page-menu color-act" markdown="1">
- [1.&nbsp;&nbsp; Bootstrap people-profit methodology](#1-launch-people-profit-processes)
- [2a. Develop groundwork technology foundation](#2a-develop-groundwork-technology-foundation)
- [2b. Define outreach minimum viable project](2b-define-outreach-minimum-viable-project)
- [3a. Research and design fullcircle solidground](#3b-research-and-design-fullcircle-groundwork)
- [3b. Research and design fullcircle today](#3b-research-and-design-fullcircle-today)
</div>

## 1. Launch people profit processes

- Status: **continual improvement**

Lay foundations for the <a href="{{ site.baseurl}}/feel/mutualism/investment-guide/" class="hover-underline"><b class="color-feel">smallcircles</b> <span class="color-think">people profit™</span></a> methodology, for continuous improvement of mutualism and synergy. This process is vital for onboarding of new circle builders, maintaining sustainable growth and keep a healthy pace to innercircles evolution.

## 2a. Develop groundwork technology foundation

- Status: [**minimum viable product**](/act/fullcircle-groundwork/requirements/){: .color-act}

Build a minimum viable product for <a href="{{ site.baseurl}}/act/fullcircle-groundwork/" class="hover-underline"><b class="color-act">fullcircle</b> <span class="color-feel">groundwork™</span></a> that targets the Fediverse community and attracts both a user base and project contributors. The functionality offers a set of federated but light-weight groupware and forum-like features.

## 2b. Define outreach minimum viable project

- Status: [**elicit requirements**](/act/teaserbot-outreach/requirements/){: .color-think}

The <a href="{{ site.baseurl}}/act/teaserbot-outreach/" class="hover-underline"><b class="color-play">teaserbot</b> <span class="color-think">outreach™</span></a> project will deliver the first domain-specific service module to be implemented on top of the Groundwork foundation. This roadmap item will investigate features and design for the minimum viable product based on stakeholder input.

## 3a. Research and design fullcircle solidground

- Status: **ongoing research**

Ongoing Research and Design for <a href="{{ site.baseurl}}/act/fullcircle-today/" class="hover-underline"><b class="color-act">fullcircle</b> <span class="color-feel">solidground™</span></a>. This roadmap item involves preparing a minimum viable product that can evolve into a full-blown residential social media network. Solidground will offer three services that build on top of each other. Development of the first service <b class="color-act">innercircles</b> <span class="color-feel">come together℠</span> will start once the Outreach MVP is launched.

## 3b. Research and design fullcircle today

- Status: **ongoing research**

Ongoing Research and Design for <a href="{{ site.baseurl}}/act/fullcircle-today/" class="hover-underline"><b class="color-act">fullcircle</b> <span class="color-feel">today™</span></a> the most complex innercircles project in terms of functionality and technology.
