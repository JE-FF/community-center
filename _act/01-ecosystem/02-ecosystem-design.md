---
permalink: /act/ecosystem/design/
title: "Ecosystem design"
excerpt: "Elaborating design and architecture highlights of our flagship projects."
sidebar:
  - image: /assets/images/site/sidebar-act.png
    image_alt: "fullcircle"
    text: 'Incubate, nourish, foster and grow sustainable circles. <b class="color-act">Let&#39;s act!</b>'
  - nav: "nav-act"
---

Here you'll find a summary of high-level design choices for the [minimum viable product](/act/ecosystem/roadmap#dogfooding-innercircle-center).

**Note**: Design choices are by no means fixed, and may change in the course of ongoing project development.
{: .notice--info}

<div class="page-menu color-think" markdown="1">
- [Development process](#development-process){: .color-act}
- [Architecture design](#architecture-design){: .color-act}
- [Technology stack](#technology-stack){: .color-act}
</div>

## Development process

We use different practices depending on the type of project, but in general we follow an organic process of [self-organization](https://en.wikipedia.org/wiki/Self-organization) and [continual improvement](https://en.wikipedia.org/wiki/Continual_improvement_process). This includes the development methods themselves..

For all development our [**mindfulness**](/about/principle/#-mindfulness){: .color-think} principle is the starting point:

1. **Success is irrelevant**, but every slight improvement counts.
2. **Failure is irrelevant**, but for the opportunity to learn.
3. **Timescale is irrelevant**, because only progress is our goal.

Together these principles free us from expectation, commitments and obligations, unless we set them for ourselves. Specific development approaches will be detailed in the project workspaces where they apply.

## Architecture design

## Technology stack

