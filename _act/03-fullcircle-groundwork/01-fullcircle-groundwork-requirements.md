---
permalink: /act/fullcircle-groundwork/requirements
title: "Groundwork project requirements"
excerpt: "An overview of the requirements of the fullcircle groundwork project."
sidebar:
  - image: /assets/images/site/sidebar-act.png
    image_alt: "fullcircle"
    text: 'Incubate, nourish, foster and grow sustainable circles. <b class="color-act">Let&#39;s act!</b>'
  - nav: "nav-act"
---

Below you'll find the minimum viable product objectives, scope and domain model.

---

<div class="page-menu color-think" markdown="1">
- [Objectives](#objectives){: .color-act}
- [Scope](#scope){: .color-act}
- [Domain model](#domain-model){: .color-act}
</div>

## Objectives

Following are the MVP objectives and success criteria:

- A usable bare-bones participation service
- Easy server installation process
- Gain first traction on the fediverse
- Attracts a user-base of first adopters
- Healthy sustainable development project

## Scope

The MVP will be developed in two stages:

1. **Foundation**: Set up bare minimum foundational layers. Get the architecture in place.
2. **Federation**: Enable federation features and support full Fediverse integration.
3. **Circles module**: Support service modules. Implement basic circles domain model.

The stages can be broken down into the following parts:

<table>
  <thead>
    <th width="33%">Stage</th>
    <th>&nbsp;&nbsp;&nbsp; Minimum requirements</th>
  </thead>
  <tbody>
    <tr>
      <td><b>1. Foundation</b></td>
      <td>
        <ul>
          <li>Stand-alone server, frontend, backend</li>
          <li>Service module architecture support</li>
          <li>User authentication, Profiles</li>
          <li>Admin interface</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td><b>2. Federation</b></td>
      <td>
        <ul>
          <li>Participate with fediverse account</li>
          <li>Federated innercircle centers</li>
          <li>Discussion threads, Reactions (basic)</li>
          <li>Mentions and Hashtags</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td><b>3. Circles module</b></td>
      <td>
        <ul>
          <li>Multiple circles, Person membership</li>
          <li>Markdown support, wysiwyg toolbar</li>
          <li>Following and Invitations</li>
          <li>Roles, Role assignment</li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

## Domain model

The following diagram depicts the core domain model that reflects functionality at stage 3 of the MVP:

![Circles domain model]({{ "/assets/images/fullcircle-groundwork-circles-domain-model-mvp.svg" | relative_url }})

**Circle**:

- A **circle** is a group where cooperation takes place.
- A circle has a purpose described as a common **interest**.
- A circle has one or more **actors** that are its members.
- A circle can have pre-defined **roles** to assign to members.

**Actor**:

- An actor is either a **participant** or another circle.
- An actor **follows** a circle to stay informed about it.
- An actor must **join** a circle in order to participate.

Not depicted in the diagram:

- An actor that follows or joined may be **notified** about activity.

**Member**:

- A **member** is an actor that joined a group.
- A member can **invite** other actors to join the circle.
- A member participates by creating **activity** in the circle.
- A member can create a **document** for other members to read.
- A member can **comment** on a document discussion thread.
- A member can **react** to documents and comments.

Not depicted in the diagram:

- A member can **mention** other actors in a document or comment
- A member can include **hashtags** in a document or comment
