---
permalink: /act/fullcircle-solidground/
excerpt: "A participation platform that allows people to interact in well-organized circles."
title: " "
seo_title: "Overview of fullcircle solidground™"
header:
  og_image: /assets/images/site/fullcircle-logo-square-white-ogimage.png
sidebar:
  - image: /assets/images/site/sidebar-act.png
    image_alt: "fullcircle"
    text: 'Incubate, nourish, foster and grow sustainable circles. <b class="color-act">Let&#39;s act!</b>'
  - nav: "nav-act"
---

<img src="{{ site.baseurl }}/assets/images/site/solidground-logo-wite-500x103.png"/>