---
layout: "posts"
entries_layout: "grid"
permalink: "/blog/"
title: "Smalltalk that circulates"
excerpt: "Our publications on what is noteworthy and interesting within our circles."
sidebar:
  - image: /assets/images/site/sidebar-talk.png
    image_alt: "smalltalk"
    text: 'Welcome to smalltalk, where small can be huge. <b class="color-think">Let&#39;s talk!</b>'
  - nav: "blog"
---

This space offers community news and highlights, and details various aspects of our activities, practices, methods and projects that will help deepen your understanding of our philosophy.