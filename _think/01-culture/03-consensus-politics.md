---
permalink: /think/culture/consensus-politics/
excerpt: "Methods of governance and decision-making where everyone counts."
title: "Consensus politics"
sidebar:
  - image: /assets/images/site/sidebar-think.png
    image_alt: "innercircles"
    text: 'Collecting knowledge to build our future. <b class="color-think">Let&#39;s think!</b>'
  - nav: "nav-think"
---
