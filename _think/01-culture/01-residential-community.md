---
permalink: /think/culture/residential-community/
excerpt: "Support of social structures in cities, towns and rural areas."
title: "Residential community"
sidebar:
  - image: /assets/images/site/sidebar-think.png
    image_alt: "innercircles"
    text: 'Collecting knowledge to build our future. <b class="color-think">Let&#39;s think!</b>'
  - nav: "nav-think"
---
