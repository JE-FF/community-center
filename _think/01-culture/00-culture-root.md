---
permalink: /think/culture/
excerpt: "Ideas and knowledge for fostering vibrant an inclusive cultures."
title: "Culture domain"
sidebar:
  - image: /assets/images/site/sidebar-think.png
    image_alt: "innercircles"
    text: 'Collecting knowledge to build our community. <b class="color-think">Let&#39;s think!</b>'
  - nav: "nav-think"
---