---
permalink: /think/culture/positive-psychology/
excerpt: "Positive aspects of human experience that make life worth living."
title: "Positive psychology"
sidebar:
  - image: /assets/images/site/sidebar-think.png
    image_alt: "innercircles"
    text: 'Collecting knowledge to build our future. <b class="color-think">Let&#39;s think!</b>'
  - nav: "nav-think"
---
