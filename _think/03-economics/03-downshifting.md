---
permalink: /think/economics/downshifting/
excerpt: "Improved balance in leisure and work by escaping economic materialism."
title: "Downshifting"
sidebar:
  - image: /assets/images/site/sidebar-think.png
    image_alt: "innercircles"
    text: 'Collecting knowledge to build our future. <b class="color-think">Let&#39;s think!</b>'
  - nav: "nav-think"
---
