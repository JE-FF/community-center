---
permalink: /think/economics/mutualism/
excerpt: "Monetary and non-monetary ways of finding mutual benefit that puts people first."
title: "Mutualism"
sidebar:
  - image: /assets/images/site/sidebar-think.png
    image_alt: "innercircles"
    text: 'Collecting knowledge to build our future. <b class="color-think">Let&#39;s think!</b>'
  - nav: "nav-think"
---
