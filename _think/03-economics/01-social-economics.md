---
permalink: /think/economics/social-economy/
excerpt: "Practices of egalitarian and participatory monetary conduct."
title: "Social and solidarity economy"
sidebar:
  - image: /assets/images/site/sidebar-think.png
    image_alt: "innercircles"
    text: 'Collecting knowledge to build our future. <b class="color-think">Let&#39;s think!</b>'
  - nav: "nav-think"
---
