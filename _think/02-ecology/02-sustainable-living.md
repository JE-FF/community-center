---
permalink: /think/ecology/sustainable-living/
excerpt: "Reduce the individual’s use of natural and personal resources."
title: "Systainable living"
sidebar:
  - image: /assets/images/site/sidebar-think.png
    image_alt: "innercircles"
    text: 'Collecting knowledge to build our future. <b class="color-think">Let&#39;s think!</b>'
  - nav: "nav-think"
---
