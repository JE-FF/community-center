---
permalink: /think/ecology/circular-economy/
excerpt: "Eliminating waste and the continual use of new resources."
title: "Circular economy"
sidebar:
  - image: /assets/images/site/sidebar-think.png
    image_alt: "innercircles"
    text: 'Collecting knowledge to build our future. <b class="color-think">Let&#39;s think!</b>'
  - nav: "nav-think"
---
