---
permalink: /think/
title: "Help inspire our circles"
excerpt: "A space to collect ideas and knowledge as input for building flourishing circles."
sidebar:
  - image: /assets/images/site/sidebar-think.png
    image_alt: "innercircles"
    text: 'Collecting knowledge to build our future. <b class="color-think">Let&#39;s think!</b>'
  - nav: "nav-think"
---

This space is for collecting <span class="color-play">ideas</span> and <span class="color-think">knowledge</span>. A library of inspiring insights and wisdom aggregated from all across the web. Waiting patiently to be adopted in delightful projects. Elaborate this knowledge base, make it better, and contribute your own exciting stuff.

<div class="notice--think" markdown="1">
  <h3 markdown="1"><span class="fas fa-trophy"></span> [Hero's triple-win:](/feel/mutualism/investment-guide/#the-heros-triple-win){: .color-think} [The Wikimedia Movement](/feel/triple-win/heroes/#-the-wikimedia-movement){: .color-feel}</h3>
  We embrace [**Wikipedia**](https://en.wikipedia.org){: .color-feel}. In all our documentation we will favour Wikipedia in knowledge references. We will improve accuracy of existing pages and create new ones for all our knowledge and insights, where applicable.
</div>

---

<div class="page-menu color-think" markdown="1">
- [Circles of sustainability](#circles-of-sustainability)
  - [Culture](#-culture)
  - [Ecology](#-ecology)
  - [Economics](#-economics)
  - [Technology](#-technology)
</div>

## Circles of sustainability

[**Sustainability**](/about/principles/#-sustainability/){: .color-act} is our most important core principle. While often associated just with 'going green', it is a much broader concept that affects all aspects of society. In innercircles we have redefined the [Circles of sustainability](/2020/09/circles-of-sustainability-redefined/){: .color-think} model for this purpose.

### <span class="fas fa-users"></span>&nbsp; Culture
{: .color-feel}

Creating vibrant communities with a strong social fabric, where diverse people can freely interact and develop themselves. Places where people are allowed to flourish. We focus on:

- [Residential community](/think/culture/residential-community/){: .color-feel}: Support of social structures in cities, towns and rural areas.
- [Positive psychology](/think/culture/positive-psychology/){: .color-feel}: Positive aspects of human experience that make life worth living.
- [Consensus politics](/think/culture/consensus-politics/){: .color-feel}: Methods of governance and decision-making where everyone counts.

### <span class="fa fa-leaf"></span>&nbsp; Ecology
{: .color-act}

To live in harmony with nature our society requires significant reductions of our ecological footprints. They should have minimal negative impact to our quality of life. We focus on:

- [Circular economy](/think/ecology/circular-economy/){: .color-act}: Eliminating waste and the continual use of new resources.
- [Sustainable living](/think/ecology/sustainable-living/){: .color-act}: Reduce the individual's use of natural and personal resources.

### <span class="fa fa-coins"></span>&nbsp; Economics
{: .color-play}

Current economic systems are big contributors to unsustainability. Promoting wealth inequality and unfair practices. We seek alternatives that stimulate economic freedom. We focus on:

- [Social economy](/think/economics/social-economy/){: .color-play}: Practices of egalitarian and participatory monetary conduct.
- [Mutualism](/think/economics/mutualism/){: .color-play}: Monetary and non-monetary ways of finding mutual benefit that puts people first.
- [Downshifting](/think/economics/downshifting/){: .color-play}: Improved balance in leisure and work by escaping economic materialism.

### <span class="fas fa-hammer"></span>&nbsp; Technology
{: .color-think}

Technology dominates our daily life, and this comes with inceasingly more negative aspects. Tech innovations must be elegant, unobtrusive and supportive to people. We focus on:

- [Socio-semantic web](/think/technology/socio-semantic-web/){: .color-think}: Rich knowledge representations based on social and human aspects.
- [Decentralization](/think/technology/decentralization/){: .color-think}: Delegating control away from central, authoritative technology platforms.
- [Small technology](/think/technology/smalltech/){: .color-think}: Everyday tools for everyday people designed to increase human welfare.
- [Humane design](/think/technology/humane-design/){: .color-think}: Design of ethical humane digital products focused on user well-being.