---
permalink: /think/technology/small-technology/
excerpt: "Everyday tools for everyday people designed to increase human welfare."
title: "Small technology"
title_color: "color-feel"
sidebar:
  - image: /assets/images/site/sidebar-think.png
    image_alt: "innercircles"
    text: 'Collecting knowledge to build our future. <b class="color-think">Let&#39;s think!</b>'
  - nav: "nav-think"
---

The principles of Small Technology were introduced by Aral Balkan and Laura Kalbag after years of focused activism and solution-building to counter Big Tech surveillance capitalism. The elegant Small Tech concepts are perfect matches for innercircles and we study them here.

**Objective**: On this page we track at a high level to which extent innercircles can align with all ten Small Technology Principles. We'll strive to improve this alignment on a continual basis. We have numbered the principles for convenience.
{: .notice--info}

---

<div class="page-menu color-think" markdown="1">
  - [Introduction](#introduction)
  - [What is Small Technology?](#what-is-small-technology)
  - [Tracking innercircles compliance](#tracking-innercircles-compliance)
</div>

## Introduction
{: .color-think}

Small Technology is positioned to be the exact opposite - the counter force - of Big Tech that is dominated by the Silicon Valley mindset of startup culture, unicorns, disruption and exponential growth. This greedy hypercapitalist environment has given rise to tech molochs that dominate the world, and are eroding society, human values and are detrimental to the world at large.

After gaining a strong understanding of [surveillance capitalism](https://en.wikipedia.org/wiki/Surveillance_capitalism) and five years of iterating on solutions to this problem, Aral and Laura founded [The Small Technology Foundation](https://small-tech.org) in 2019. Besides advocating and promoting the principles, they are researching and developing free software to showcase the principles in practice. One such application is the [SiteJS](https://sitejs.org/) construction set, which extends their principles towards the [Small Web](https://ar.al/2020/08/07/what-is-the-small-web/).

## What is Small Technology?
{: .color-think}

The concept is about 'small and simple' and on the foundation website it is defined as follows:

> _"Small Technology are everyday tools for everyday people designed to increase human welfare, not corporate profits."_

Small technology defines ten fundamental principles for information technology to adhere to:

> [![The 10 Principles of Small Technology](/assets/images/think/small-tech/small-tech-10-principles.png)](https://small-tech.org/about/#small-technology)
>
> _source: [About Small Technology](https://small-tech.org/about/#small-technology)_

## Tracking Small Tech compliance
{: .color-think}

Below we track overall innercircles compliance and how we achieve and sustain them. The icons indicate compliance level:

<i class="far fa-check-circle color-act"></i> Compliance engrained in our [**Principles**](/about/principles){: .color-feel}.<br>
<i class="far fa-dot-circle color-think"></i> Continual improvement and monitoring.<br>
<i class="far fa-circle color-synergy"></i> No full compliance in the short term.

---

### <i class="far fa-check-circle color-act"></i> Principle 1: Personal
{: .color-act}


### <i class="far fa-dot-circle"></i> Principle 2: Easy-to-use
{: .color-think}


### <i class="far fa-check-circle color-act"></i> Principle 3: Non-colonial
{: .color-act}


### <i class="far fa-dot-circle"></i> Principle 4: Private-by-default
{: .color-think}


### <i class="far fa-dot-circle"></i> Principle 5: Zero knowledge
{: .color-think}


### <i class="far fa-circle color-synergy"></i> Principle 6: Peer to peer
{: .color-synergy}


### <i class="far fa-check-circle color-act"></i> Principle 7: Share alike
{: .color-act}


### <i class="far fa-dot-circle"></i> Principle 8: Interoperable
{: .color-think}


### <i class="far fa-check-circle color-act"></i> Principle 9: Non-commercial
{: .color-act}


### <i class="far fa-check-circle color-act"></i> Principle 10: Inclusive
{: .color-act}