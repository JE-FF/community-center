---
permalink: /think/technology/humane-design/
excerpt: "Design of ethical humane digital products focused on user well-being."
title: "Humane design"
sidebar:
  - image: /assets/images/site/sidebar-think.png
    image_alt: "innercircles"
    text: 'Collecting knowledge to build our future. <b class="color-think">Let&#39;s think!</b>'
  - nav: "nav-think"
---

<div class="notice--play" markdown="1">
  <h3><span class="fas fa-theater-masks"></span> delightful resource</h3>
  We maintain [**delightful humane-design**](https://codeberg.org/teaserbot-labs/delightful-humane-design){: .color-play}. A curated list of humane design best-practices, information and code projects.
</div>