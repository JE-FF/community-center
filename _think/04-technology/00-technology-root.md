---
permalink: /think/technology/
excerpt: "Ideas and knowledge for technology innovations that benefits humanity."
title: "Technology domain"
sidebar:
  - image: /assets/images/site/sidebar-think.png
    image_alt: "innercircles"
    text: 'Collecting knowledge to build our future. <b class="color-think">Let&#39;s think!</b>'
  - nav: "nav-think"
---
