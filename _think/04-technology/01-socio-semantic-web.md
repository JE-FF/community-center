---
permalink: /think/technology/socio-semantic-web/
excerpt: "Rich knowledge representations based on social and human aspects."
title: "Socio-semantic web"
sidebar:
  - image: /assets/images/site/sidebar-think.png
    image_alt: "innercircles"
    text: 'Collecting knowledge to build our future. <b class="color-think">Let&#39;s think!</b>'
  - nav: "nav-think"
---

<div class="notice--play" markdown="1">
  <h3><span class="fas fa-theater-masks"></span> delightful resource</h3>
  We maintain [**delightful linked data**](https://codeberg.org/teaserbot-labs/delightful-linked-data){: .color-play}. A curated list of semantic web technology standards, information and code projects.
</div>