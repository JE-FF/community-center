---
permalink: /think/technology/decentralization/
excerpt: "Delegating control away from central, authoritative technology platforms."
title: "Decentralization"
sidebar:
  - image: /assets/images/site/sidebar-think.png
    image_alt: "innercircles"
    text: 'Collecting knowledge to build our future. <b class="color-think">Let&#39;s think!</b>'
  - nav: "nav-think"
---
