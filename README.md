# innercircles community center

This is the project from where the community center website is developed and maintained.

## Extending the site

See the tutorials on [minimum mistakes](https://mmistakes.github.io/minimal-mistakes) and its [github repository](https://github.com/mmistakes/minimal-mistakes) for details on the site structure and features. Additionally you can find information on using [Jekyll](https://jekyllrb.com/docs/), the [Liquid](https://shopify.github.io/liquid/) template language, and [Kramdown](https://kramdown.gettalong.org/) markdown parser.

### Omit page title

You can omit a page's `<h1>` title by not specifying the `title` in the frontmatter. But if you do that, you MUST specify a SEO title:

```yaml
seo_title: "The page title for SEO usage"
```

### Slide decks

To add an embedded slide deck to a page, add the following to its frontmatter:

```yaml
page_layout: "deck"
```

In the contents section of the page you can immediately start adding `<section>` elements to define the slides. Refer to the [RevealJS](https://revealjs.com) for information on the markup format to use.

When viewing a slide deck presentation, click the presentation and press '`s`' for full-screen and '`?`' for help with keyboard shortcuts.

## Contributing

Check out the [Open Source Guide](https://opensource.guide/) for general information about contributing to open source.

### Contributing Code

If you'd like to help build the website itself, read the [code contribution guidelines](contributing-code.md) on how to get started.

### Contributing Content

If you'd like to write a blog post, share research and other resources, or contribute other content to the site, read the [content contribution guidelines](contributing-content.md) to find out how.

## Attribution

- Website is created with [Jekyll](https://jekyllrb.com/).
- Based on [Minimal Mistakes](https://mmistakes.github.io/minimal-mistakes/) theme (v4.13.0) by Michael Rose.
- Slide decks are created with [RevealJS](https://revealjs.com).

## License

[AGPL-3.0](LICENSE)
