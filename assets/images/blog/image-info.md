# Photo attributions

Hero images have been modified to be of size 1600 x 550 pixels.

## Landing page hero

Used on the landingpage and in "2020-08-28-essence-of-innercircles" blog post.

- Image source: https://commons.wikimedia.org/wiki/File:Happy_children_splashing_water.jpg
- Image license: CC0 Public domain
- Image creator: Sasin Tipchai

## 2020-09-01-circles-of-sustainability-redefined

- Image source: https://www.pexels.com/photo/scientist-checking-crops-in-laboratory-3912479/
- Image license: Free to use without attribution
- Image creator: https://www.pexels.com/@thisisengineering

## 2020-09-25-fifth-state-of-optimism

- Image source: https://www.pexels.com/photo/happy-woman-removing-face-mask-after-taking-bath-3852159/
- Image license: Free to use without attribution
- Image creator: [Anna Shvets](https://www.pexels.com/@shvetsa)

## 2020-11-05-lifeshifting-to-flourishing

- Image source: https://www.pexels.com/photo/bloom-blooming-blossom-blur-462118/
- Image license: CC0 Public Domain
- Image creator: [Pixabay](https://www.pexels.com/@pixabay)

## 2020-11-06-how-people-profit

- Image source: https://www.pexels.com/photo/photo-of-people-sitting-near-wooden-table-3182796/
- Image license: Free to use without attribution
- Image creator: https://www.pexels.com/@fauxels