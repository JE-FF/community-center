---
title:  "Circles of sustainability redefined"
title_color: "color-think"
excerpt: "In innercircles we embrace the circles of sustainability model. But with some important twists."
header:
  image: /assets/images/blog/2020-09-01-circles-of-sustainability-redefined/pexels-thisisengineering-3912479.jpg
  teaser: /assets/images/site/innercircles-logo-white-teaser.png
  og_image: /assets/images/site/innercircles-ogimage.png
author: Arnold Schrijver
grid_color: "color-think"
last_modified_at: 2020-09-01T09+02:00
---

## Circles of sustainability
{: .color-think}

Sustainable growth and long-term viability of our species are of utmost importance for humanity to be still around in the far future. We are now on a path of destruction, and that must change. In innercircles we strive for holistic sustainability, and loosely follow the [circles of sustainability](https://en.wikipedia.org/wiki/Circles_of_Sustainability) model.

However we redefined this model to better fit the innercircles philosophy and align better to our [**Sustainability**](/about/principles/-#sustainability){: .color-act} core principle. The sweet spot in this model is in the center, where there the right balance can be found between the four domains.

Our redefinition of the traditional model:

- Omits the **politics** domain in its entirety.
- Adds **technology** as a sustainability domain.
- Ranks **culture** and **ecology** higher than **economics**.

And here is how that looks like in a diagram:

<div style="text-align:center;" markdown="1">
  ![innercircles circles of sustainability]({{ "/assets/images/innercircles-circles-of-sustainability.png" | relative_url }})
</div>

### <span class="fas fa-users"></span>&nbsp; Culture
{: .color-feel}

Learning how people interact. This is where most of our work is focused. Our solutions help people improve life, enable them to [flourish](https://en.wikipedia.org/wiki/Flourishing) more easily. We focus on [the self](https://en.wikipedia.org/wiki/Self), our lifestyle and reinforcing social bonds.

### <span class="fa fa-leaf"></span>&nbsp; Ecology
{: .color-act}

In our world the rich have way too large of an ecological footprint, while the poor who work hard for meager incomes, don't have the luxury to care for the environment. We look for practical sustainability solutions that help on both ends.

### <span class="fa fa-coins"></span>&nbsp; Economics
{: .color-play}

Going from big business to small business. We look for small-scale microeconomics practices that work to strengthen people within our circles. In this we strive to de-emphasize the role of money and materialism. We maximize human value, not material profit.

### <span class="fas fa-hammer"></span>&nbsp; Technology
{: .color-think}

We need humane technology that is ubiquitous, unobtrusive, and non-extractive. We create 'technology for people', which we refer to as [small tech](/think/technology/small-technology/){: title="humane technology for people"}.

## Technology as a sustainability domain?
{: .color-think}

Technology in our world is pervasive. It underlies all the other domains of Culture, Ecology and Economics. But the way in which our tech evolves follows paths that, as you are probably aware, are increasingly detrimental to humanity. New innovations come with many negative side-effects on society.

While Big Tech molochs dominate and set the rules. Driven by greed they lock us up in walled gardens. Then in a deceitful web of [surveillance capitalism](https://en.wikipedia.org/wiki/Surveillance_capitalism) they harvest our data for extractive purposes. They decide what innovations trickle down. As a result technology access has become very unequally distributed. Hardware production is no better. It sees people work in harsh unfair conditions, while being hugely destructive to the environment.

Online social media networks feed negativity loops, help erode the truth. Even to the extent where they erode our social fabric and undermine democracy worldwide. Technology is increasingly driving us towards a dystopic future.

> **_"While we’ve been upgrading our technology we’ve been downgrading humanity."_**
> — Center for Humane Technology, [The Problem](https://humanetech.com/problem/)

Like the Center for Humane Technology, founded by Tristan Harris, we will work to put a stop that these negative trends. We support the vision of the [Humane Tech Community](https://humanetech.community) of:

> **_Ubiquitous Humane Technology that Stimulates Humans to Flourish and Humanity to Thrive."_**
> — Humane Tech Community, [Our vision](https://community.humanetech.com/t/3322)

Due to the importance of technology to society we have defined it as a separate domain of the Circles of sustainability model, and we will address solutions that lead to beneficial technology innovations. Technology that is elegant and delightful, ubiquitous and unobtrusive and supportive to mankind.

We'll seek partnerships with the Humane Tech Community and many other similar efforts. To share knowledge and to bundle our powers.

## So where's the Politics domain?
{: .color-think}

Politics is part of the original Circles of sustainability model, but as you've noticed it not present in our adapted version. Why is that? Well, the answer is simple. 

> **_"We don't practice traditional politics at innercircles community."_**

Nor do we in the circles we build. The reason is that, especially at larger scales, politics tends to become power play where hidden motives are involved. Politics all too easily leads to moral corruption of individuals and groups. That, however, does not mean that we don't recognize the important role politics plays in society. But these days people, rightly or wrongly, have mostly negative associations with it.

So we avoid dealing with politics for the most part, especially where it concerns our group of circle builders. This will set us free and help remove barriers, stimulate our creative processes. We do however cooperate with federal bodies at all levels of government to gather support for our initiatives.

With regards to the solutions we design and build we'll seek for better alternatives that exist. Solutions able to replace the intruments of power that politics uses. We'll adopt as these best-practices wherever we can. 

For example for the governance of online and residential community circles we'll explore the field of [consensus politics](/think/culture/consensus-politics/). Best practices might then subsequently be integrated in our <a href="/act/innercircle-center/" class="color-think"><span>**innercircle**</span> <span class="color-feel">center™</span></a> flagship project.