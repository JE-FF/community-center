---
title:  "What is innercircles in essence?"
title_color: "color-think"
excerpt: "You can describe innercircles in many ways. Most accurately put its about discovering how to play."
header:
  image: /assets/images/Happy_children_splashing_water.jpg
  teaser: /assets/images/site/fullcircle-logo-white-teaser.png
  og_image: /assets/images/site/fullcircle-logo-square-white-ogimage.png
author: Arnold Schrijver
grid_color: "color-act"
last_modified_at: 2020-08-28T18+02:00
---

**Many people say _"Life is but a game"_. That is easily uttered when you are prosperous and all is well. But life can be harsh depending on chance and circumstances. In innercircles we practice the [the game of life™](/about/){: .color-feel} so that we can learn to become better players. And we help others to learn that too.**

This article explains some high level innercircles concepts for those who are interested. A brief summary can also be found in our [helicopter view](/2020/08/helicopter-view-of-innercircles/){: .color-think} slide deck.

## Where it started
{: .color-act }

As a young kid I was fascinated by science and technology. I often dreamt about the wonderful things that humanity would achieve in the near future. That fascination still exists to an extent. But now there is doubt and even scepticism of where technology innovation is leading us to. The future sure doesn't look all that bright to me anymore.

My biggest motivation to found innercircles was my intense and growing frustration about how we tackle the big problems of our world. Or more precisely not tackle them. Collective inaction or slowness to act while the situation grows worse and worse. Why aren't we able to address these issues? 

## Why big solutions come slowly
{: .color-act }

I came to see that there are two important reasons at play here. First of all us humans get all too easily lost when dealing with complex systems. So we lose sight of the big picture and start to focus on small chunks of the problem space. The ones we find easier to handle. In a way this is fine, because all progress and improvements count. But we should beware not to fight the symptoms, and instead always focus on the cause. I find that mostly we do not do that, and instead give most attention to symptoms like climate change or rising populism. In my opinion the root cause is that the _entire system_ that modern society is based on is **inherently unsustainable**. To anyone with common sense it is clear to see that a system in pursuit of infinite growth, endless wealth and a survival-of-the-fittest mechanism is doomed to fail in the long run. It is like a Ponzi scheme really, and does not match with an earth that has only bounded resources.

The second reason of inaction plays out on a more personal level. Most people are well aware about our problems and how to solve them. But we are in general not inclined to act in any real and fundamental ways until there is true urgency and direct impact to our own lives. Or we might simply not have the time to spare, involved as we are in hectic rat-race daily life. Also we tend to point to others to blame and shift responsibility to. The argument goes somewhat along the lines of: "Why should I sacrifice more than another? Look at those big polluters over there. Let them improve their ways first! I am just a small fry in the pan.". While an understandable reaction, the argument is wrong. Because every action, however tiny, is still one that counts.

## Simple solutions still exist
{: .color-act }

The two fundamental aspects depicted above are what witholds us from a brighter future. And they are the reasons why I started innercircles. I believe that fundamentally our big problems still have easy solutions.

The root cause of inherent unsustainability provides innercircles with a [**Vision**](/about/vision/){: .color-think}: The quest for holistic sustainability. Holistic here refers to all aspects of society, namely **economics**: sustainable business and work environments, **ecology**: living on smaller footprints, **culture**: a strong social fabric, and **technology**: humane, unobtrusive and supportive to all three other areas. Together we call these the _circles of sustainability_.

> [**Sustainability**](/about/principles/#-sustainability){: .color-act} is one of the four core principles of innercircles. It addresses the root cause of our societal problems.

The barrier to take decisive action is what provides innercircles with its [**Mission**](/about/mission/){: .color-act}: To provide a methodological way forward, that makes being part of the solution the most logical step to take. Where holistic sustainability plays on a societal level, for the individual it revolves around lifestyle. Changing ones way of life to better align with the grand scheme of things. All people want to live happy, fulfilling lives and do the things that are important to them. The quest to give meaning to life is a very personal one, where everyone has different dreams and aspirations.

> [**Happiness**](/about/principles/#-happiness){: .color-feel} is the second core theme of innercircles. Making the fruits of life easier to obtain for each and everyone.

So the mission of innercircles then is to find and create sustainable solutions that make people happier. One part of these solutions are concerned with a set of methods and practices we develop and evolve. This study track on the subject of 'happiness behavior' is called [**flourishing life**](/feel/flourish/){: .color-feel}. When practiced together with circles of sustainability they give access to a new approach to life: to lead an [**evolutionary lifestyle**](/feel/flourish/lifestyle/){: .color-act}. One where we live life to its full potential and in harmony with our surroundings.

The other part of the solutions we build deal with more practical works. These can be supportive technologies, like open source software projects that become part of our [**ecosystem**](/act/ecosystem/){: .color-act}, or they might be awareness campaigns and social experiments conducted in the [**teaserbot realm**](/act/ecosystem/#-the-teaserbot-realm){: .color-play}.

As we build our solutions the participants involved will not just have the satisfaction of building something good and proper for humanity. These circle builders, as we call them, will be the first to benefit from their work themselves. The way we work, our methodologies, ensure this. But this still leaves unexplained why these solutions would be in any way simple in how they address big problems.

## Mutualism and synergy
{: .color-act }

Many hands make light work and strength comes in numbers. In order to make solving big challenges easier, innercircles will have to grow. Hence all our methods, projects, principles and grassroots organization structure are designed to scale. Additionally and importantly, in order to overcome the 'barrier to act', they are set up to be as attractive as possible for people to participate.

Wouldn't you love to contribute to a good cause where you aren't just asked to make sacrifices in terms of time, energy and possibly money? Where the movement you joined had first and foremost the objective to give you something valuable back in return of your efforts? This is the goal that innercircles has set for itself. That circle builders reap personal benefit from their participation with others. We do this by practicing mutualism in all our relationships.

Mutualism effectively means creating a win-win situation, where all parties involved get a net benefit. We seek win-win situations _everywhere_. And it doesn't stop there, because usually when there's a win-win situation the opportunity exists to make it even better. To find extra synergy to be gained from it. When we do this, in innercircles terms we have now created a [**triple-win**](/feel/triple-win/){: .color-feel} situation.

The methodical pursuit of triple-wins with circle builders and partners or other people is called [**people profit**](/feel/mutualism/){: .color-feel}. The term 'profit' is an intentional wink to the more business-like concept we associate it with. People profit involves continuously optimizing mutual benefit and **human value** in a virtuous cycle of maximizing synergy.

> [**Humanity**](/about/principles/#-humanity){: .color-act} is one of our two instrinsic values. We will prove the enormous benefit and plain common sense of _truly_ putting human value above all else.

The emphasis on human value in synergetic relationships is the special sauce in our methodology, and is what sets it apart from other similar approaches. This starts for instance with the trust-first approach we embrace. Or we may foster partnerships with others who aren't even aware of it yet, in so-called _one-sided triple wins_. 

> _**“So what is this radical idea? That most people, deep down, are pretty decent.”**_<br/>
> _― Rutger Bregman, Humankind: A Hopeful History_

We strongly believe most human beings are good. And our approach will test-drive that belief against reality. You could call that one of the social experiments we conduct.

## Mindfulness principles
{: .color-act }

Lofty goals and visions of bright utopic possible futures are all good and well. But they are worth exactly nothing, if they are unattainable and in practice totally out of reach. Nonetheless many organizations and movements fall into the trap of making them, and find their members disappointed with the lack of progress over time.

So innercircles has defined their vision a bit differently: _“Come together, come fullcircle, grow, become whole.”_. You may notice that this simple text, which encompasses our core principles, is applicable at any scale. Because scale in the end isn't all that important. Being part of the solution and making small positive steps forward is. Whether they be personal improvements or collective ones, it is only progress that really matters.

> [**Mindfulness**](/about/principles/#-mindfulness){: .color-think} is our third core principle. It dictates that what happens, just happens. How much we can achieve depends on the amount of minds we manage to bring together.

Adoption of the mindfulness principle has some really nice implications. It means first of all that the pressure for success is effectively eliminated. Our success criteria are just progress, however tiny. Failure is eliminated too. It only counts as a learning experience.

 There are also no guarantees where we're headed, as growth is entirely organic. We do not actively advertise our community. This website is even blocked for search engines. If you are reading this you either bumped onto innercircles by coincidence, or were introduced by a friend or acquaintance. In innercircles we don't have a real membership concept. If you contribute then you are a circle builder. What this means is entirely up to you. Participation comes without any obligations, nor expections. This is not only mindful, but touches on the second important value we hold dear.

> [**Freedom**](/about/principles/#-freedom){: .color-think} is our second intrinsic value. We deeply value the four freedoms: Freedom of mind, freedom of spirit, freedom of choice and freedom of expression.

As a result of all this flows that we don't work with set timescales and schedules. Time is unimportant too in the face of progress. That doesn't mean we do not have roadmaps, strategic planning and iterative project development. But these are planned on a case-by-case basis, depending on opportunity and people willing to commit.

## Play the game. Search for treasure
{: .color-act }

Finally I will now tell you a little bit about the fun part. You are gonna love this, I think..

As a final incentive to make people stick with innercircles once they become involved, we strive to make participation as fun and joyful an experience as we can. This is reflected in the type of projects that we build, which aim to elicit delight and wonder. The open culture of friendliness, optimism and positivity is a second aspect. And we will go on adventures, conduct experiments, wander exciting new paths, explore radical concepts. These will involve artists, craftsmen and people from all professions and walks of life, with different backgrounds and cultures. We'll make friends from all around the world. Many of these will take place under the guidance of **teaserbot labs™**, one of our four 'brands' that has the objective to gently _"tease, nudge and play"_.

Back to the beginning: Life is but a game. And in this light innercircles can be seen as a cooperative gameplay team that together plot game strategies and build tools that result in better outcomes in life. This realization means that you can consider yourself a game designer. As circle builder you help design The Game of Life. So let your creativity go wild. Remember, success is irrelevant, failure unimportant. This leaves full freedom to bring fun, joy, adventure, love and other delightful elements into play.

> [**Playfulness**](/about/principles/#-playfulness){: .color-play} is the fourth and final core principle of innercircles. Relearn how to play, just like children do, with wild imagination. And make your dreams come true.

What are your ambitions in this game? What are _your_ treasure chests and lootboxes? How do you place them? What great adventures do you want others to experience on a path you guided them to? Intriguing to think about, isn't it.. do you want to be a circle builder?

Let's play.<br/>
Let's only dream!