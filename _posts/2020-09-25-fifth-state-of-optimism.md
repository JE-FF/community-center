---
title:  "The Fifth State of Optimism"
title_color: "color-feel"
excerpt: "A practical way to go forward with optimism and positivity where only progress counts."
header:
  image: /assets/images/blog/2020-09-25-fifth-state-of-optimism/pexels-anna-shvets-3852159.jpg
  teaser: /assets/images/site/smallcircles-logo-white-teaser.png
  og_image: /assets/images/site/fullcircle-logo-square-white-ogimage.png
author: Arnold Schrijver
grid_color: "color-feel"
last_modified_at: 2020-09-25T12+02:00
---

**In innercircles we follow an approach where _only progress counts_{: .color-think}. This is part of our [Mindfulness]({{ "/about/principles/#-mindfulness" | relative_url }}){: .color-think} principles. A very powerful concept that can be readily applied to ones personal life to adopt a mindset of optimism and positivity.**

Going from these principles, one can go one step further and adopt: **The Fifth State of Optimism**{: .color-feel}

When only progress counts, however small, this means that success is eliminated. That is a liberating starting point. Furthermore by considering failure as a learning experience, any setbacks can also be eliminated as a negative force. Following from this model we see that timescale is now also unimportant, alleviating us of time pressure.

### The Pitfall of Pessimism
{: .color-feel}

When one is aware of what is going on around us in our world, follows the news as a critical thinker, it is very easy to become mired in doom & gloom. To become pessimistic about humanity's outlook, and even become outright cynical in our perception of the world. This state is not only depressive, but causes actual stress and anxiety. It can affect our social relationships, and how we perceive and interpret the motivation and intent of other people in negative ways.

It is a trap. A vicious cycle of negativity that drags us down. We are no longer able to value positive developments as they are: _positive_. It has a stifling effect on all our actions that might lead to progress. In general it creates **barriers to flourishing**.

I have personally experienced the Pitfall of Pessimism. Having a long-time interest in geo-politics and intrigued by questions such as _"What are real motives why countries go to war?"_. Then my involvement for 2 years in raising awareness about Big Tech problems for the Humane Tech Community, and seeing that most people don't really care, or are too busy to give it their attention. And to top it off there's the complete lack of progress I witnessed over the last 35 years in how we solve environmental issues and the destruction of our planet. All stuff that tends to make one pessimistic.

I became this cynical guy, and could've turned into that typical grumpy old man or lady stereotype that everyone knows examples of. I wanted out of this negative spiral!

### Towards Positive Realism
{: .color-feel}

The good news is that an entirely uplifting virtuous cycle of hope and progress also exist, if only you allow it to take hold within yourself.

I planned to be someone that goes forward with positivity and optimism, no matter the situation. This is, of course, an ongoing quest. So far, walking this path, has gone quite well for me. I have experienced changes that brighten my every day. The change started with the recognition that **there is always hope**. Always!

The trick is to adopt an overall positive, solution-oriented mindset. Going from a glass-half-empty to a glass-half-full person. This was easier to adopt than I thought. How? Just by doing and practicing. By looking around and actively absorb positivity. It starts by seeking a positive thought to each negative one that pops up in your head. Then you can let go of the negative one and follow-up on that positive inkling. Gradually this will twist things around having positive thoughts becomes much easier. Your mindset changes. Positivity comes natural.

I also realized there is only so much awareness of a problem one needs to have. Just enough to allow you to tread the path of a solution. The rest can be tuned out for the time being, to avoid getting stifled by it. The problem analysis is mostly already done by others, and can be used as input to solution building as need requires.

This 'tuning out' and only keep the actionable part is where progress is found. It is 'being in the here and now'. It is practicing **practical mindfulness**.

What do I mean by that? I have followed a mindfulness training, which I really enjoyed. Yet for me all the meditation-like exercises, such as the so-called bodyscan and breathing exercises, just did not stick. Your experience may vary, of course. But overall it is the practical philosophy - the way of thinking - that is important to grasp. It is applicable in all our day-to-day activities and can become engrained in our habits.

Also I am convinced they can be applied to technology as well: By creating **mindful technology** that is intuitive, ubiquitous and unobtrusive. Technology that supports us, but doesn't get in the way. But I digress, as this post is not about technology.

A positive mindset plus practical mindfulness taken together leads one to be in a state of **positive realism**, or realistic positivity.

### Fifth state of optimism
{: .color-feel}

No matter where you are in your mental attitude, on the side of optimism or having pessimistic tendencies, there is always the lure to go further into that direction than what is prudent. We all know people that are overly optimistic, or take the gloomiest possible view. Like I said above, it is a spiral that takes you further along. Whether we are optimistic or realistic means we are biased, and we seek confirmation bias in support of our views.

There is a pitfall to optimism too: People with unrealistic goals and visions, who believe in utopic futures and fairy-tale outcomes to their initiatives. Their optimism might not even lead to disappointment when objectives do not materialize. But this can be very discouraging to the people they convinced to go along with them on their ride.

In innercircles positive realism is the path we want to walk. There are no lofty, utopic visions, only dreams that provide direction and lead to opportunities and open up possibilities. This is the state of mindfulness that exists between _glass-half-empty_ and _glass-half-full_ mindset, representing the balance between them. And it is far removed from the extremes of _fullblown gloominess_ and _fullblown bliss_. It is the "Fifth State of Optimism". A concept we will adopt in [**Flourishing life**](/feel/flourish){: .color-feel}.

A summary of this concept is depicted as follows:

![Fifth State of Optimism]({{ "/assets/images/blog/2020-09-25-fifth-state-of-optimism/072_linkedin_fifth-state-of-optimism.png" | relative_url }})

This image I created in 2017 as a so-called _teaser tile_ when ideating teaserbot's _"tease, nudge, play"_ approach. It was published to my [LinkedIn](https://www.linkedin.com/in/arnoldschrijver/) profile and [Teaserbot](https://twitter.com/teaserbot) on Twitter (but I have removed them since).

Mind your realism, embrace your optimism 😃