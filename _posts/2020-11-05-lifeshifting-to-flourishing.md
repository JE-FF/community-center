---
title:  "Lifeshifting Pathways to Flourishing"
title_color: "color-feel"
excerpt: "Re-balancing our lifestyle to circles of sustainability and flourishment best-practices."
header:
  image: /assets/images/blog/2020-11-05-lifeshifting-to-flourishing/pexels-pixabay-462118.jpg
  teaser: /assets/images/site/fullcircle-logo-white-teaser.png
  og_image: /assets/images/site/fullcircle-logo-square-white-ogimage.png
author: Arnold Schrijver
grid_color: "color-act"
last_modified_at: 2020-11-05T09+02:00
---

**_Finding The Path to the Inner Circle is the quest make life more meaningful and fulfilling, and live in harmony with others and our surroundings. By shaping pathways that lead to sustainability and happiness. We achieve this by the practice of "lifeshifting"._**

Lifeshifting is the process of adopting an [**evolutionary lifestyle**](/feel/flourish/lifestyle){: .color-feel} - a way of living that leads to a brighter future - and one of the tools to set us out on leading a more [**flourishing life**](/feel/flourish){: .color-feel}.

> **lifeshifting** : re-balancing our lifestyle according to circles of sustainability and flourishment best-practices.

In this article I will address the [**Sustainability**](/about/principles/#-sustainability){: .color-act} aspects of lifeshifting. This starts by slightly redefining the existing concept of **downshifting** and introduce the new concept of **upshifting**. The latter is a better fit for people that are not in the luxurious position to be able to downshift. We'll evaluate the rationale of choosing one or the other.

## Shifting up
{: .color-act}

Wikipedia defines [downshifting](https://en.wikipedia.org/wiki/Downshifting_(lifestyle)) as follows:

> The term downshifting in the English language refers to the act of reducing the gear ratio while driving a vehicle with manual transmission. In the context of social behavior or trend, the term relates to individuals living simpler lives to escape from what critics call the rat race.
>
>The long-term effect of downshifting can include an escape from what has been described as economic materialism, as well as reduce the "stress and psychological expense that may accompany economic materialism". This new social trend emphasizes finding an improved balance between leisure and work, while also focusing life goals on personal fulfillment, as well as building personal relationships instead of the all-consuming pursuit of economic success.

While this definition has a lot of good in it, it should be clear that the whole concept of _downshifting_ applies best to people in the privileged Western world. People that live in democratic countries and have decent, stable incomes and pensions, healthcare and social safety nets.

For the poor in general and most people in rich privileged countries things are entirely different. There is no "economic materialism" and "stepping out of the rat-race" in the Western meaning of the word. People have to scrape a living together and work incredibly hard. There's no rat-race, but a fight for survival. Oftentimes people in the working class are severely exploited by ruthless enterprises and corrupt elites. Note that oftentimes these are headquartered in or part of a supply chain originating from privileged countries.

Poor people don't need to downshift. To the poor hypercapitalism demonstrates its ugliest, monstrous face in all aspects of life. For these people _upshifting_ is urgently needed: An escape from economic oppression, unfair labour conditions, towards better living conditions and equal chances similar to those people in the West can count on. To move from surviving to thriving.

> **Note**: Talking about 'Western' countries is inaccurate. The often used terminology of 'developed' vs. 'under-developed' regions is avoided, because of its negative connotation with development aid. We use privileged and under-privileged instead.

The focus below with regards to upshifting is on being poor in under-privileged areas of the world. Poverty in privileged countries deserves separate attention and different strategies.

## Upshifting versus downshifting
{: .color-act}

Both upshifting and downshifting are valid lifeshifting approaches one can follow to improve their outlook on leading a more flourishing life. Depending on your life conditions you choose the predominant strategy to follow, but it is encouraged to mix'n choose best-practices from the both strategies. 

Important is to gradually adopt a firm _mindset_ and _determination_ to shift your life, and to counter the forces of society that always work to direct you back what counts as 'mainstream' in your living environment. Note that lifeshifting is a much broader concept than upshifting or downshifting alone.

Downshifting in innercircles is defined as:

> ## Downshifting
>
>Improving opportunities to flourish by diminishing economic materialism and consumerism, and the pressures of society that come with that, by adopting more sustainable lifestyle practices.
>
>Downshifting highlights _awareness_.

Here's how innercircles defines upshifting:

> ## Upshifting
>
>Improving opportunities to flourish by gaining strength and independence against forces of oppression that dominate your life and inhibit sustainable growth, by adopting sustainable alternatives to the festering malpractices that work to keep you down.
>
>Upshifting highlights _empowerment_.

Upshifting and downshifting lend themselves real well for education programs. In both these definitions the strategies provide more 'room to flourish' i.e. to allow you more time to spend on lifeshifting, where you can _think_ and _act_ on shaping paths to happiness.

## Sustainable shifts
{: .color-act}

So both upshifting and downshifting are related to sustainability. Let's look at each of the innercircles [**Circles of sustainability**](/2020/09/circles-of-sustainability-redefined/){: .color-act} in turn, and briefly highlight the relationship.

### Economics circle
{: .color-play}

If you are downshifting you'll be pondering questions such as "What is the definition of success in life?", "How much money do I really need to be happy?". You might evaluate and change your career path in a direction that will give your life more meaning.

People that are upshifting first start to evaluate the many ways in which they are captured in poor and unhealthy working conditions. From that analysis they start looking at strategies that allow them to break free. Questions raised are "How can we organize against oppressing forces?" and "How can we extract ourselves and find a living in alternative lines of work where we are in control?".

### Ecology circle
{: .color-act}

For downshifters there's the question of "What are the many ways in which I am polluting the environment, and what can I do about them?". Well-to-do people have consumer power, and their consumption patterns matter. Many people see downshifting as *sacrifice*, i.e. giving up on things and living a poorer life. This is a misconception, and becoming more aware of nature, to care more for nature and to live in harmony with nature is immensely rewarding.

Shifting up entails to large extent finding protection against the many exploitative practices to the local environment by external power players. Our voracious global economy has an unsatisfiable need for resources, and these are extracted via corrupt supply chains without care for the environmental destruction. Another aspect of upshifting here is arming the local population with modern knowledge to increase crop yields, avoid soil erosion, and restore depleted ecosystems to former glory.

### Culture circle
{: .color-feel}

When shifting down one evaluates how their participation in the hectic society does impact their social and emotional life. There are many reasons to escape the 'rat-race', but first it is essential to become aware what you are missing out on while rushing along. Modern society with its warped focus on what's important has a way to make us forget about the low-hanging fruits of life, while we aim for 'pie in the sky'.

The situation with regards to culture in under-privileged regions is very dire. The poison of hypercapitalism here is literally destroying the social fabric of communities, eradicating local cultures and ancient traditions. False dreams of 'becoming more like the West' are portrayed, and a bleak, warped mirror image of Western lifestyle and consumerism is forced upon the people. The key focus of upshifting is to protect and retain local identities and habits, and instead of mindless adoption of capitalist practices, create translations of these to the local context and only when they make sense.

### Technology circle
{: .color-think}

Downshifting means having a real critical eye on the role of technology in modern-day life. We have adopted some truly crazy technology practices and mindless tech adoption has become a religion. This can be seen in modern trends such as 'digital transformation' as a key goal. Or the ignorant acceptance of the total annihilation of our privacy, which will be the future foundation for plutocratic elites to dominate us in ways we find very hard to imagine. Technology use has to be brought back to supportive and unobtrusive roles, where it purely serves as an enrichment to life's non-technical practices.

In under-privileged areas of the world the dystopic technologies that Big Tech is working on, are being rolled out, test-drived and improved right now. There are no rules here, no morals and ethics nor regulation to keep tech in check. Here extreme high-tech is introduced to very low-tech areas, and the people are defenseless against it. There are many, many good applications of technology here, where the people can benefit from. And they provide great opportunities if the local population is able to master them themselves. Awareness, defense, education and introduction by locals, not foreigners, is key to improving outlook here.