---
title:  "Explaining how people profit"
title_color: "color-act"
excerpt: "Learn about our methodology to maximize synergy and benefit from our collaboration."
header:
  image: /assets/images/blog/2020-11-06-how-people-profit/pexels-fauxels-3182796.jpg
  teaser: /assets/images/site/smallcircles-logo-white-teaser.png
  og_image: /assets/images/site/fullcircle-logo-square-white-ogimage.png
author: Arnold Schrijver
grid_color: "color-feel"
last_modified_at: 2020-11-06T23+02:00
---

**_Our hypercapitalist society gradually de-humanizes us in ways we are mostly unaware of. Like frogs in boiling water we do not notice how our values erode, as newspeak replaces human language. Here we'll look at "Profit" and redefine how people profit._**

## Profit newspeak
{: .color-feel}

Let's first look how the dictionary defines profit:

> [**prof·it**](https://www.thefreedictionary.com/profit) _(prŏf′ĭt)_
>
> 1. An advantageous gain or return; benefit.
> 2. Financial gain from a transaction or from a period of investment or business activity, usually calculated as income in excess of costs or as the final value of an asset in excess of its initial value.

The first definition is pretty intuitive, right? The second one though.. wow, what an abstract description. This is the modern materialistic redefinition that has come to dominate the meaning of the word. If someone says: _"If you help me with this, then we can both profit."_ there will be dollar signs in your head as you consider the offer.

This should not be how it works. Like 'value' we have come to think of 'profit' mostly in monetary terms. Something measured in dollar amounts, wealth and status. Like all warped newspeak language, in innercircles we set a goal to redefine this terminology to its original intent, and adapt it to human scale. We do this by applying common wisdom and our common sense.

Though an 'advantageous gain' can be one-sided, an _advantageous return_ indicates a win-win situation. If we combine the latter with 'benefit', then we could redefine "profit" as a _mutualism concept_. And this is exactly what we do in innercircles when we talk about [**People profit**](/feel/mutualism/){: title="The innercircles method to maximize mutualism and synergy." .color-feel}.

## What is people profit?
{: .color-feel}

Mutualism is central to innercircles and core to all of our activities. It is all about helping each other to move forward in life, and this is always to our mutual benefit.

> **_"It is one of the beautiful compensations in this life that no one can sincerely try to help another without helping himself."_**
> _− Ralph Waldo Emerson, philosopher_

People profit is the holistically 'extended version' of our materialistic, shallow definition of profit. And it is based on mutualism and the concepts of 'giving and taking' and maintaining a 'balance sheet' of sorts. Hence it includes monetary exchanges to obtain goods and services that we are familiar with. And all forms of non-monetary exchanges, such as [Bartering](https://en.wikipedia.org/wiki/Barter).

But more importantly it includes exchanges of _human values_. You can think about [Gifting](https://en.wikipedia.org/wiki/Gift) or [Altruism](https://en.wikipedia.org/wiki/Altruism) here, but it actually goes much deeper than that. We consider the whole range of human emotions and feelings that are involved in our relationships with others.

Giving and altruistic deeds - whether anonymous or not - always returns something of value. Not the kind you find on balance sheets and in financial calculations, but true human value. Value which is immeasurable. This usually starts with gratitude, but can blossom into so much more. Like friendship, appreciation, kindness and love. All positive emotions are at play in this, and they are key to flourishing.

## Decent human values
{: .color-feel}

In innercircles we go from the assumption that _"Most people are pretty decent"_, and this forms the basis of our trust-first approach towards other people. This is also the premise on which [Rutger Bregman](https://www.rutgerbregman.com/) based his book ["Humankind: A Hopeful History"](https://www.bloomsbury.com/uk/humankind-9781408898932/).

> **_"Brace yourself for the most dangerous idea yet: most people are pretty decent."_**
> _− Rutger Bregman, [Writing in The Correspondent](https://thecorrespondent.com/443/brace-yourself-for-the-most-dangerous-idea-yet-most-people-are-pretty-decent/58612083398-55f77a22)_

With decent people involved, this is where [**People profit**](/feel/mutualism){: .color-feel} enters the game. Circle builders strive to maximize the intrinsic human value that is involved in their cooperation with others. By doing this methodically in all our interactions, we create the conditions that enable us to also maximize the [synergy](https://en.wikipedia.org/wiki/Synergy) of our collaboration.

Circle builders engage in a virtuous cycle of increasing mutualism and synergy in all relationships, and we emphasize human-value-based exchanges in this process. The assumption, which is one of innercircles' social experiments, is that these exchanges have a higher 'yield' than mere cold-hearted business-like transactions.

> **Synergy**: An interaction or cooperation giving rise to a whole that is greater than the simple sum of its parts.

How can human values be the 'currency' in a value exchange? This is simple, and it is a concept we all know and practice in daily life. If you help your neighbor with some chore, you do not present a bill afterwards. Neither do you expect your neighbor to immediately return a favour to balance things out. Instead you exchanged values, where you provided a service and you received a host of human values in return. Gratitude, appreciation, respect, loyalty, etcetera. These are now on your ‘balance sheet’, and when later on you need your neighbor to help you they will gladly do so.

Note that ‘balance sheet’ is a poor analogy that refers the cold business world. Things are more intricate where true human values are involved. In many cases people don’t need anything tangible in return for a deed. The gratitude and appreciation may be sufficient ‘payment’. They make you feel good, and this satisfaction provides the counterbalance.

## The virtuous synergy cycle
{: .color-feel}

So we optimize mutual benefit and human value is our currency of flourishment. The people profit methodology consists of two parts. An activation mechanism to bring new circle builders into play. This is our growth strategy. After onboarding they engage with others in a virtuous cycle of cooperation that focuses on synergy creation. These parts taken together facilitate a process we call **continuous evolution**.

![People profit methodology](/assets/images/innercircles-people-profit-overview.png)

As it happens the steps in the people profit diagram correspond with consecutive parts of [**Mission**](/about/mission/){: .color-act} and [**Vision**](/about/vision/){: .color-think} of innercircles. Let's evaluate how this works.

The activation process is related to the Mission. If you want to realize dreams, then:

- **Start with yourself (act)**: Individual initiative is key to life and world improvement.
- **Start small**: Dreams can guide us but only progress counts, however small.
- **Activate others**: Call the help of your friends and be stronger together.
- **Grow**: Cooperate, teach and learn our collective knowledge and life experience.

The synergy is created in a positive feedback loop, consisting of the steps _"think, act, feel and play"_, which relate to our Vision and reflect our [**Principles**](/about/principles/){: .color-feel}:

- **Come together _to think_{: .color-think}**: Apply common knowledge, common wisdom, common sense.
- **Come fullcircle _by our acts_{: .color-act}**: Move towards the circles of sustainability sweet spot.
- **Become whole _in our feelings_{: .color-feel}**: By reflecting on our thoughts and corresponding acts.
- **Grow _by playing_{: .color-play}**: Freely combine our thoughts, acts and feelings, and flourish.

We value [**Freedom**](/about/principles/#-freedom){: .color-think} above all. Seeking people profit is voluntary and should be fun and rewarding. Therefore the last part of our Mission is **"Just dream and play"**.

Finally the part of the Vision that refers to _Becoming Whole_ has both a personal aspect, which is mentioned above. The other aspect is the collective outcome of innercircles: The societal and environmental benefits that we brought to the world by our collaboration.

## The power of the triple win
{: .color-feel}

With an ever growing number of circle builders and partners it is vital to have an efficient mechanism to track and optimize our mutual relationships with others, and find the opportunities that allow us to maximize synergy. This mechanism is called a [**Triple win**](/feel/triple-win/){: .color-feel}.

> **triple win**: A long-lasting relationship between parties, who seek win-win situations with extra synergy on a continual basis.

Creating win-win situations happens in life and business all the time. Looking for extra synergy requires a more holistic approach to fostering relationships. This approach is found in mutualism based on a re-appreciation of human values as described above.

With an eye on our trust-first approach and to do away with formalisms from the commercial business world we can also describe triple wins as:

> **triple win**: A continually reinforced mutual beneficial trust relationship between parties that avoids having to use instruments that are based on *distrust*, such as formal NDA's, contracts and lawyer involvement.

The mutualism in the triple win is the assurance that all parties will stick to their commitments of help to the others involved. If the triple win becomes too one-sided, i.e. unbalanced, or if one of the parties betrays the others, then the mutual benefit diminishes or entirely evaporates. The chain of trust may be damaged or broken altogether.

With well-thought-out social economy best-practices adopted within innercircles, there's _a lot of_ mutual benefit that can flow from triple wins this way, without having to resort to formal business constructs. But for larger triple wins, and especially those involving commercial parties there will be a mix'n-match of innercircles and traditional business instruments (e.g a light-weight legal framework) to uphold the triple win.

In the continual quest to maximize the synergy of the mutual beneficial relationship, to further a triple win, we ask questions such as:

- Given our relationship and level of trust, what else can we cooperate on and how can we combine it with what we already do?
- Given the other triple wins that I am evolving, are there matches where I can combines these in stronger or additional triple wins?

## De-emphasizing the role of money
{: .color-feel}

The triple win also allows us to re-evaluate the role of money. We should realize that money in principle is merely a tool to ease and facilitate the transfer of 'value'. Where value means goods and services. The money itself is only a 'promise of value', nothing more. But it comes with many negative 'side-effects', such as greed and avarice.

A [barter economy](https://en.wikipedia.org/wiki/Barter) is a _"system of exchange where participants in a transaction directly exchange goods or services for other goods or services without using a medium of exchange."_

With all the evil that money brings along, why don't we have a barter economy? Well, for one thing it is really inefficient. I have to know what other people have to barter, and when I want to barter something the other party needs to be interested in what I have to offer. Secondly this system does not scale well to large groups of people. These are the reasons money was invented in the first place.

But with the triple wins we foster within innercircles there's renewed opportunity to barter. After all, everything that is in the triple win are already things that all parties involved are interested in. The exchange of goods and services (deeds) need not involve money here. It is optional.

To be more specific:

> **triple win**: The _medium of exchange_ of value within a triple win is an optimal combination of money on the one hand, where this is most efficient, and _human values_ on the other hand, where the nature of the trust relationship allows it. Human value in this concept is the preferred currency.

This definition describes how and why we want to de-emphasize the role of money.

## Providing basics support
{: .color-feel}

With [**People profit**](/feel/mutualism/){: title="Ways to benefit from innercircles" .color-feel} and our core philosopy of pursuing a [**Flourishing life**](/feel/flourishing/){: title="The life philosophy we develop" .color-feel} we can reduce the role money plays in our lives. To show that there are better ways than always watching the bottom line. Yet we are not utopists. A certain amount of income is a requirement to provide for the bare necessities in our life. The quenching of our basic needs is the pre-condition of flourishment. We call this [**Basics support**](/feel/support/){: title="Support for those in our circles" .color-feel}.

All the activities and services provided by innercircles will be entirely non-commercial, either offered for free, at cost price, or in return for other services as part of triple win mutual benefit exchanges. As the innercircles community grows its _circles of influence_ - its ethos - and more people join as circle builders, we'll be able to provide basics support to those in need of help.

Until that time we need some basic income ourself to sustain and drive our evolution and growth. You can [**Support our circles**](/feel/donate/){: title="Financial support for innercircles itself" .color-feel} to cover operating costs and make frugal but smart investments. We do this - naturally - with full transparency.

Initially our funding comes in the form of donations, sponsoring and subsidies provided by people and organizations that align with our principles, philosophy and worldview. As soon as we are able to we will find other sources of revenue that allow us to stand on our own.