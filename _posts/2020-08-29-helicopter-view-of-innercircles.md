---
layout: "deck"
seo_title: "Helicopter view of innercircles"
excerpt: "A walkthrough along high-level concepts. How we solve big problems using simple solutions."
header:
  teaser: /assets/images/site/innercircles-logo-white-teaser.png
  og_image: /assets/images/site/innercircles-ogimage.png
grid_color: "color-think"
last_modified_at: 2020-05-29T20+02:00
---

<section>
  <p><img src="{{ '/assets/images/decks/innercircles-logo-explainer.svg' | relative_url }}"/></p>
</section>

<section>
  <h2>we have big problems</h2>
  <p>
    <ul class="fragment">
      <li><b>economics:</b> extractive hypercapitalism</li>
      <li><b>ecology:</b> planet-scale deterioration</li>
      <li><b>culture:</b> erosion of our social fabric</li>
      <li><b>life:</b> rushing by in an eternal rat-race</li>
    </ul>
  </p>
</section>

<section>
  <h1>root cause</h1>
  <h2 class="fragment">inherent unsustainability</h2>
</section>

<section>
  <h1 class="color-think">obvious solution</h1>
  <h2 class="fragment color-act">holistic sustainability</h2>
</section>

<section>
  <p><img src="{{ '/assets/images/innercircles-circles-of-sustainability.png' | relative_url }}"/></p>
</section>

<section>
  <h1 class="color-think">so let's work on that</h1>
  <h2 class="fragment color-feel">simple solutions still exist</h2>
</section>

<section>
  <p><img src="{{ '/assets/images/decks/innercircles-logo-square.svg' | relative_url }}"/></p>
</section>

<section>
  <h2 class="color-think" style="text-align: center">smallest approach</h2>
  <p class="fragment" style="text-align: center">
    start with yourself<br/>
    activate others<br/>
    start small<br/>
    grow<br/>
    <b class="fragment color-act">act</b>
  </p>
</section>

<section>
  <h2 class="color-think">simple process</h2>
  <p>
    <ul class="fragment">
      <li class="color-think"><b>think</b> - <span style="color: #3d4144">common wisdom, common sense</span></li>
      <li class="color-act"><b>act</b> - <span style="color: #3d4144">by common people and for progress</span></li>
      <li class="color-feel"><b>feel</b> - <span style="color: #3d4144">positive emotions, human values</span></li>
      <li class="color-play"><b>play</b> - <span style="color: #3d4144">make learning easy, have some fun</span></li>
    </ul>
  </p>
</section>

<section>
  <h2 class="color-think">two methods <b class="color-feel">one outcome</b></h2>
  <p>
    <ul class="fragment">
      <li class="color-think">through collective action: <span class="color-feel"><b>people profit</b></span></li>
      <li class="color-think">by personal participation: <span class="color-feel"><b>flourishing life</b></span></li>
      <li class="fragment color-act" style="font-weight: bold; margin-top: 1em; list-style-type: '➜';">&nbsp; an evolutionary lifestyle</li>
    </ul>
  </p>
</section>

<section>
  <p><img src="{{ '/assets/images/decks/innercircles-people-profit.png' | relative_url }}"/></p>
  <p class="fragment color-think">continuously optimizing <b>mutual benefit</b><br/>and <span class="color-feel">human value</span> in a virtuous cycle<br/>of maximizing <span class="color-play">synergy</span></p>
</section>

<section>
  <p><img src="{{ '/assets/images/innercircles-people-profit-overview.png' | relative_url }}"/></p>
</section>

<section>
  <p><img src="{{ '/assets/images/decks/innercircles-flourishing-life.png' | relative_url }}"/></p>
  <p class="fragment color-think">gradually adopt a <b>way of living</b> that<br/>promotes <span class="color-feel">harmony</span> between people<br/>and brings <span class="color-act">balance</span> to the world</p>
</section>

<section>
  <p><img src="{{ '/assets/images/innercircles-flourishing-life-overview.png' | relative_url }}"/></p>
</section>

<section>
  <h2 class="color-think">innercircles is</h2>
  <p style="padding-left: -5em">
    <ul class="fragment">
      <li class="color-think">sharing knowledge</li>
      <li class="color-act">collective action</li>
      <li class="color-feel">value humans</li>
      <li class="fragment color-play">realizing dreams</li>
    </ul>
  </p>
</section>

<section>
  <h2 class="color-think">motivations to participate</h2>
  <p>
    <ul class="fragment">
      <li class="color-think">I want to learn new skills and crafts</li>
      <li class="color-act">I want to help improve the world</li>
      <li class="color-feel">I want to know more about myself</li>
      <li class="color-play">I want to have fun, meet new friends</li>
    </ul>
  </p>
</section>

<section data-transition="slide-in fade-out">
  <h2 class="color-think">" an investment vehicle<br/>for <span class="color-play">your dreams</span> "</h2>
</section>

<section data-transition="fade-in">
  <h2 class="color-think">so ..</h2>
  <h1 class="fragment color-play">dare to dream</h1>
  <h2 class="fragment color-think">circle builder</h2>
</section>