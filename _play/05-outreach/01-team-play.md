---
permalink: /play/outreach/teamplay/
title: "Team up with others"
excerpt: "."
sidebar:
  - image: /assets/images/site/sidebar-play.png
    image_alt: "teaserbot labs"
    text: 'Come onboard, spread the word, master the game. <b class="color-play">Let&#39;s play!</b>'
  - nav: "nav-play"
---