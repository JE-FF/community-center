---
permalink: /play/
title: "Help expand our circles"
excerpt: "This space explains how you can join the game and help expand our radius."
sidebar:
  - image: /assets/images/site/sidebar-play.png
    image_alt: "teaserbot labs"
    text: 'Come onboard, spread the word, master the game. <b class="color-play">Let&#39;s play!</b>'
  - nav: "nav-play"
---

We combine our <span class="color-think">intelligence</span> and <span class="color-feel">social</span> skills in mutual beneficial ways. In this space you learn how to create opportunities. Craft your _path to the inner circle_ and play the game of life with us.

---

<div class="page-menu color-play" markdown="1">
  - [Get On Board](#get-on-board)
  - [Tease](#tease)
  - [Nudge](#nudge)
  - [Play](#play)
  - [Reach Out](#reach-out)
</div>

## Get on board

As circle builder being part of innercircles can bring you great benefit and exciting opportunities. Emerge yourself in our activities, participate and shape your own initiatives. Our [**Freedom**](/about/principles/#-freedom){: .color-think} and [**Mindfulness**](/about/principles/#-mindfulness){: .color-think} principles mean you are fully in charge to shape your own desired path.

- [**Join Us**](onboarding/join){: .color-play}: Discover how to shape opportunities, get further in life and flourish some more.
- [**Partnering**](onboarding/partners){: .color-play}: Let's be stronger together. Bring your own initiatives and projects into play.
- [**Players wanted**](onboarding/team-up){: .color-play}: Find where we are actively looking for participants to jump in the game.

## Tease

Where other organizations are involved in public relations and marketing, we practice the **gentle tease** to bring awareness to our cause. [**Playfulness**](/about/principles/#-playfulness){: .color-play} will be an important part of innercircles.

- [**Gamification**](tease/gamify){: .color-play}: Playing is the best way to learn. Not just for children. We learn how to play.

<div class="notice--play" markdown="1">
  <h3><span class="fas fa-theater-masks"></span> teaserbot.. tease, nudge, play!</h3>
  It is crazy that [learning through play](https://en.wikipedia.org/wiki/Learning_through_play) refers only to children. In our future we foresee delicious projects involving the Arts and our **social intelligence** to make people more aware of what it means to be human.
</div>

## Nudge

We do not grow by advertisement or public relations, but increase our numbers by inviting our friends. So they can benefit with us. Others are selectively approached or met and introduced by chance based on intuition. Our nudge campaigns help us to find candidates.

- [**Poke a friend**](nudge/invite){: .color-play}: Get your friends on board and introduce them to the benefits of innercircles.
- [**Activate others**](nudge/invite){: .color-play}: Help find circle builders and partners for current activities and projects.
- [**Campaigning**](nudge/campaigns){: .color-play}: Participate in awareness campaigns to highlight innercircles concepts.

## Play

As circle builder you are part of the game. We build innercircles together. Participation is most fun and productive where your dreams and aspirations match common goals and ongoing action. But our scope is vast. Take the initiative and scan for action to get the highest return.

- [**Scan for action**](game/action){: .color-play}: Find ongoing work, campaigns and projects where you can jump in.

## Reach out

Collaborative teamplay and co-creation are powerful tools. We encourage you to share your dreams with other circle builders. To find common ground, form a team, and start realizing them. Unleash your creativity and think out-of-the-box. Your dream too wild yet? Then first cloutsurf it.

- [**Form a team**](outreach/teamplay){: .color-play}: Find circle builders and partners to help further your plans.
- [**Cloutsurf ideas**](outreach/cloutsurfing){: .color-play}: Express your idea in ways to maximize The Power of Pull.