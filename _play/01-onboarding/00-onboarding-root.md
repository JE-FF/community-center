---
permalink: /play/onboarding/
title: "Get on board"
excerpt: "We love to welcome you among us as a new circle builder or partner."
sidebar:
  - image: /assets/images/site/sidebar-play.png
    image_alt: "teaserbot labs"
    text: 'Come onboard, spread the word, master the game. <b class="color-play">Let&#39;s play!</b>'
  - nav: "nav-play"
---

Welcome dear circle builder or partner, it is lovely to see you here. Let's discover opportunities of mutualism and synergy. May your participation in innercircles be a most exciting experience.

<div class="page-menu color-play" markdown="1">
  - [Be a circle builder](#become-a-circle-builder)
  - [Partner with innercircles](#partner-with-innercircles)
  - [Jump right in with us](#jump-right-in-the-game)
</div>

## Become a circle builder

As soon as you contribute to innercircles, you are circle builder for life. We value progress, however small, while we maximize mutual benefit that flows from our collective efforts. Learn what it means to [**Join us**](join/){: .color-play}.

## Partner with innercircles

Are you on a parallel path to ours? Do we have common goals or do some of our objectives align? Can we complement each other in interesting ways? In innercircles we do not compete. Only cooperation counts, while we co-create the future. Let's be [**Partners**](/partners/){: .color-play}.

## Jump right in the game

We have many interesting ongoing activities, research and projects under development. Whatever your interest, aspirations and dreams may be, there is opportunity to work towards them. Discover existing places in our [**ecosystem**](/act/ecosystem/){: .color-act} where we are [**Looking for players**](team-up/){: .color-play} or [**Scan for action**](/play/game/action/){: .color-play} yourself.