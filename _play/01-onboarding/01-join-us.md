---
permalink: /play/onboarding/join/
title: "Be a circle builder"
excerpt: "."
sidebar:
  - image: /assets/images/site/sidebar-play.png
    image_alt: "teaserbot labs"
    text: 'Come onboard, spread the word, master the game. <b class="color-play">Let&#39;s play!</b>'
  - nav: "nav-play"
---

Here you find what circle building means, and how we create [**People profit**](/feel/mutualism){: .color-feel} together.

<div class="page-menu color-play" markdown="1">
- [Setting up the board](#setting-up-the-board)
- [Playing the game](#playing-the-game)
- [There is no competition](#there-is-no-competition)
- [There are no rules](#there-are-no-rules)
- [There is only more fun](#there-is-only-more-fun)
</div>

## Setting up the board

We do not have the non-inclusive concept of _membership_. Being human is more than enough. We all have the same goals in mind: to make the most of life. To improve it, make it better where we can, for ourselves and those dear to us. Our quest is to find what makes life meaningful.

> **_Find The Path to The Inner Circle_**
> _− innercircles, Main Quest_

As you know in the game of life there are many deeper levels to be found. There is 'treasure' and 'lootboxes' all abound. The innercircles philosophy allows you to discover them. The methodology that we use and extend strives to make your participation as fun and intrinsically rewarding as we can. It provides opportunities to reap great benefit from your participation.

## Playing the game

According to [**Mindfulness**](/about/principles/#-mindfulness){: .color-think} principles nothing is expected from you as circle builder. Your freedom is total, and you can be inactive for as long as you wish. That also means that you will not benefit as much as you could. In innercircles you shape your own opportunities by taking the initiative:

1. **Introduce**: Introduce yourself to other circle builders. Discuss, interact and meet.
2. **Share**: Share dreams and aspirations in a [**triple win**](/feel/triple-win/){: .color-feel}. Start with modest ones.
3. **Collaborate**: Team up in places where your interest lies. Or build your own team.
4. **Invest**: Start earning [**personal gains**](/feel/triple-win/me){: .color-feel} and evolve your own triple wins.

Learn more about investing in [**How people profit**](/2020/08/how-people-profit/){: title="Maximize mutual benefit and synergy of your cooperations." .color-feel} and our [**Investment guide**](/feel/mutualism/investment-guide/){: .color-feel}. To start collaborating look for [**Players wanted**](../team-up/){: .color-play} or just [**Scan for action**](/play/game/action/){: .color-play} across the board.

We play a cooperative game and are on a common path. Any contribution, however small, makes you a circle builder. In our approach we hope you'll stick around and build with us. Both for yourself and the person that comes next to you. This way together we'll co-create our future.

## There is no competition

So many like-minded individuals, groups and organizations everywhere are on a similar proper path. We learn from others, share our wisdom, and forge [**Partnerships**](../partners/){: title="Existing partners and how to join as partner" .color-play} where we can.

> **_Only Compete Against Hurdles To Overcome_**
> _− innercircles, Main Challenge_

We take non-competitiveness to the extreme, by testing the hypothesis that our [**Principles**](/about/principles/){: .color-feel} and [**Evolutionary lifestyle**](/feel/flourish/lifestyle/){: .color-feel} can 'outplay' anyone playing the Game of Power - of materialism and greed. And this only by presenting an alternative, more attractive choice and otherwise just not competing with them. We play a different game.

## There are no rules

That is right. No rules, but the ones we choose to live by ourself. Non-binding ones and accepted by free will, they are mere guidelines we decide to follow. There is beauty in that concept. It constitutes a path in itself. A path to human flourishing, where only freedom rules.

> **_"No rules exist, and examples are simply life-savers answering the appeals of rules making vain attempts to exist."_**
> _− André Breton, French surrealist and poet_

If innercircles knows no rules, then how do we defend against those that try to spoil the game, the wannabe cheaters of which there are so many in this world? That is simple - in theory at least - as these people, by their cheats, will place themselves beside our board. Out of play, and unable to reap the rewards we do, share our treasure.

Our implicit ruleset is that of human value, the morals and ethics that make us who we are, and which cannot be written down. We can just follow our [**Principles**](/about/principles/){: .color-feel}.

## There is only more fun..

No rules, no commitments, no obligations nor expectation, no need to win. Timeless. Success is irrelevant. Only the mere joy and rewards of participation is what remains. That lowers the barriers to come and play - to be like children and have some fun with us - don't you think?

<div class="notice--play" markdown="1">
  <h3><span class="fas fa-theater-masks"></span> a note on having fun..</h3>
  The **_"fun"_** in innercircles is not a shallow concept. It encompasses every activity that brings us closer to the fruits of life. The fun or learning new things, interacting with others and making new friends. It's about rediscovering the joys of life.
</div>

Herein then lies our growth formula. One that should automatically attract more people to join the game. Of course we want to facilitate as many players as we can, but never more than we can handle. Slow sustainable growth in this regard is king. Hence we first only [**Poke our friends**](/play/nudge/invite/){: title="Invite friends to be circle builders" .color-play} and only gradually commence to engage broader audiences.

> **_"Play as much as you can as often as you can with as many people as you can. That's how you learn and grow."_**
> _− Les Claypool, American musician, author and actor_

By doing this well and strategically we assure there is only more fun to come, while we only gently _tease, nudge and play_ with our vision - our dreams and aspirations - in front of us. With progress our only objective they can only come closer to realization, as failure is irrelevant too.

Be mindful of that realization.