---
permalink: /feel/triple-win/heroes/
title: "Honoring our heroes"
title_color: "color-think"
excerpt: "We thank these true exemplars of humanity with our Hero's triple wins."
sidebar:
  - image: /assets/images/site/sidebar-feel.png
    image_alt: "smallcircles"
    text: 'Radiate our support. Help people help people thrive. <b class="color-feel">Let&#39;s feel!</b>'
  - nav: "nav-feel"
---

Learn about the projects, initiatives and organizations that are true exemplars in fields that innercircles is active in, and that are sublimely aligned with the [**Principles**](/about/principles/){: .color-feel} we hold dear.

---

<div class="page-menu color-feel" markdown="1">
  - [The Wikimedia Movement](#-the-wikimedia-movement)
  - [The UN Sustainable Development Goals](#-the-un-sustainable-development-goals)
  - [The Free Software Movement](#-the-free-software-movement)
  - [The Fediverse](#-the-fediverse)
</div>

## Our Hero's triple wins
{: .color-think}

These are the triple wins created solely for the purpose to honour our heroes. By contributing to our [**Hero’s triple wins**](/feel/mutualism/investment-guide/#the-heros-triple-win){: .color-feel} we show gratitude for all the good that our heroes bring into this world.

### <span class="fa fa-trophy"></span>&nbsp; The Wikimedia Movement
{: .color-feel}

<p markdown="1">
  [![Wikimedia Projects Overview](/assets/images/triple-wins/hero-wikimedia/wikimedia-projects-overview.png)](https://wikimedia.org)
</p>

### <span class="fa fa-trophy"></span>&nbsp; The UN Sustainable Development Goals
{: .color-feel}

<p markdown="1">
  [![UN Sustainable Development Goals Overview](/assets/images/triple-wins/hero-un-development-goals/SDG_Poster_2019.png)](https://www.un.org/sustainabledevelopment/)
</p>

### <span class="fa fa-trophy"></span>&nbsp; The FOSS Movement
{: .color-feel}

<p markdown="1" style="padding: 2em;">
  [![Free Software Foundation Logo](/assets/images/triple-wins/hero-free-software-movement/logo-fsf.org.svg)](https://fsf.org)
</p>

**Note**: Though the [The Free Software Foundation](https://fsf.org) is mentioned here, we support the [Free Software Movement](https://en.wikipedia.org/wiki/Free_software_movement) in its entirety and its notion of FLOSS, which we feel is best represented by the FSF.
{: .notice--info}

### <span class="fa fa-trophy"></span>&nbsp; The Fediverse
{: .color-feel}

<p markdown="1" style="padding: 2em;">
  [![Fediverse Logo](/assets/images/triple-wins/hero-fediverse/Fediverse_logo.svg)](https://fediverse.party)
</p>