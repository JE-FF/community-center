---
permalink: /feel/
title: "Help embrace our circles"
excerpt: "The space dedicated to having people flourish in all of our community circles."
sidebar:
  - image: /assets/images/site/sidebar-feel.png
    image_alt: "smallcircles"
    text: 'Radiate our support. Help people help people thrive. <b class="color-feel">Let&#39;s feel!</b>'
  - nav: "nav-feel"
---

This space is about <span class="color-act">flourishing</span> and designing paths to <span class="color-feel">happiness</span>. Exploring preconditions to living well with others and the world around us, we focus on human value and personal growth.

---

<div class="page-menu color-feel" markdown="1">
- [Triple wins](#triple-wins){: .color-feel}
- [People profit](#people-profit){: .color-feel}
- [Flourishing life](#flourishing-life){: .color-feel}
- [Basics support](#basics-support){: .color-feel}
</div>

## Triple wins

A triple win is a win-win situation with extra synergy and part of the [**people profit**](people-profit/){: .color-feel} methodology. We seek them _everywhere_ and track them so maximum synergy results from our cooperations.

- [**Hero's triple wins**](triple-win/heroes/){: .color-feel}: Our hero projects and initiatives that we'll help in any way possible.
- [**Partner profits**](triple-win/partners/){: .color-feel}: Overview of our partnerships and how we strive to help each other.

We highly encourage you as circle builder or potential partner to look for triple wins in your participation with innercircles. What dreams and desires can you combine with our goals?

- [**Personal gains**](triple-win/me/){: .color-feel}: A tutorial on how you can benefit the most from innercircles.

## People profit

Mutualism and synergy. In innercircles our first objective is direct personal benefit of circle builders and partners. Maximization of synergy on a continual basis is core to our crowdsourced **people profit** methodology. Emphasis is on human values and a trust-first approach.

- [**Investment guide**](mutualism/investment-guide/){: .color-play}: Reference documentation for the people profit method.
- [**Investopedia**](mutualism/investopedia/){: .color-play}: Examples of mutual beneficial cooperations to inspirate your triple wins.

## Flourishing life

Both a mindset and a practical methodology, flourishing life is an approach for people to get more out of life. Based on our collective wisdom and life experience we crowdsource best-practices and design pathways to a happier, more meaningful life.

- [**Explore flourishing**](flourish/philosophy/){: .color-act}: Learn more about the philosophy of flourishing and the good life.
- [**Flourishment practice**](flourish/practice/){: .color-act}: Practical ways to improve your life and that of others.
- [**Evolutionary lifestyle**](flourish/lifestyle/){: .color-act}: How to gradually adopt a harmonious, well-balanced lifestyle.

## Basics support

This community center, the 'inner circle' of innercircles, is only one of many other circles we'll launch cooperatively. It forms the first supporting pillar of the innercircles philosophy and practices. We'll continue to add services to support you in our common quest to help others.

<div class="notice--feel" markdown="1">
  <h3><span class="fas fa-hands-helping"></span> smallcircles.. foundation for flourishment</h3>
  Proper support is instrumental for the good life. We really need [**your help**](donate/){: .color-synergy}, so we can help [**expand your radius**](triple-win/me/){: .color-feel} in return.
</div>