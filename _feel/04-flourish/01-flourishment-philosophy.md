---
permalink: /feel/flourish/philosophy/
title: "Thoughts on flourishment"
excerpt: "."
sidebar:
  - image: /assets/images/site/sidebar-feel.png
    image_alt: "smallcircles"
    text: 'Radiate our support. Help people help people thrive. <b class="color-feel">Let&#39;s feel!</b>'
  - nav: "nav-feel"
---



## The game of life

## Path to the inner circle

## Small is huge