---
permalink: /feel/flourish/
title: "Flourishing life"
excerpt: "."
sidebar:
  - image: /assets/images/site/sidebar-feel.png
    image_alt: "smallcircles"
    text: 'Radiate our support. Help people help people thrive. <b class="color-feel">Let&#39;s feel!</b>'
  - nav: "nav-feel"
---

TODO

## The philosophical aspect

On a more esoteric, deeper level, there is a life philosophy involved. Its purpose is to find meaning and direction in life in a unique and down-to-earth manner. Our purpose is not to teach a new way of thinking at all, or tell people how to behave. Instead - by going along with us and participating on the practical side - valuable insights and wisdom are gradually gleaned and woven into our purpose and future direction.

Every person is equal, and brings their own perspective - their life experience - along with them. Together we'll nourish our philosophy, make it stronger, while we apply it in real life. This is a natural process. Life's wisdom, after all, is shared by all of us. The simple credo is: