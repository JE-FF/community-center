---
permalink: /feel/mutualism/investment-guide/
excerpt: "Profit measured in human value and personal growth. Together we can maximize it."
title: " "
seo_title: "How people profit"
sidebar:
  - image: /assets/images/site/sidebar-feel.png
    image_alt: "smallcircles"
    text: 'SRadiate our support. Help people help people thrive. <b class="color-feel">Let&#39;s feel!</b>'
  - nav: "nav-feel"
---

![People Profit]({{ "/assets/images/site/innercircles-people-profit-lightbox.png" | relative_url }})

Welcome to People Profit, an investment strategy for life. We measure profit in terms of human value and personal growth.

**Note:**{: .color-feel} This is a living document, a methodology updated in a continual improvement process.
{: .notice--warning}

**Investopedia:**{: .color-act #investment-strategy} **Investment strategy** ➜ a set of guidelines, behaviors or deeds, collected to guide a persons's choices on how to best spread their humanity. [See [old definition](https://en.wikipedia.org/wiki/Investment_strategy)]
{: .notice--success}

**Investopedia:**{: .color-act #investment-vehicle} **Investment vehicle** ➜  a way of investing human value alongside other investors in order to benefit from the inherent advantages of working as part of a group. For example innercircles is an investment vehicle for dreams. [See [old definition](https://en.wikipedia.org/wiki/Investment_vehicle)]
{: .notice--success}

---

<span class="color-feel">• </span>[How people profit](#how-people-profit){: .color-feel} <br/>
<span class="color-feel">• </span>[What are your dreams?](#what-are-your-dreams){: .color-feel} <br/>
<span class="color-feel">• </span>[Mutualism and synergy](#mutualism-and-synergy){: .color-feel} <br/>
<span class="color-feel">• </span>[Continuous evolution](#continuous-evolution){: .color-feel} <br/>
<span class="color-feel">• </span>[Find the triple win](#find-the-triple-win){: .color-feel} <br/>
<span class="color-feel">• </span>[Gifting and gift wrapping](#gifting-and-gift-wrapping){: .color-feel} <br/>
<span class="color-feel">• </span>[Matchmaking and marriage](#matchmaking-and-marriage){: .color-feel} <br/>
<span class="color-feel">• </span>[Self-disclosure and debiasing](#self-disclosure-and-debiasing){: .color-feel} <br/>
<span class="color-feel">• </span>[Tease, nudge, and play](#tease-nudge-and-play){: .color-feel} 

## How people profit

People profit are gains measured in human value. Every person is precious, a treasure, born to exchange value. Unlike banks creating money from thin air, we do this without incurring debt. Our basic shareholder advice for people to follow is:

> **_"Invest in others with human interest, to reap benefits, maximize yield."_**

**Investopedia:**{: .color-act #yield} **Yield** ➜ human value returned to the gifter in terms of appreciation returned by the gift's recipient. [See [old definition](https://en.wikipedia.org/wiki/Yield_(finance))]
{: .notice--success}

## What are your dreams?

That is the central question you need to ask yourself as circle builder. We put people first when we create _"simple solutions for human flourishing"_. So that logically includes you who dedicate yourself to this noble task. You flourish first, our projects come secondary.

> **_"Living your dreams is the path to make them real."_**

So what are your dreams? Strangely enough many people do not have a ready answer to that. It helps to start small and think of the little dreams. But never lose sight of your grand visions.

### Express your dreams

 Together with us you can help realize your dreams if you set your mind to it. Just enough focus to maintain direction is enough. Learn to express them to other circle builders. Let them know where you want to be in life, because only then you allow others to anticipate them.

### Act on your desires

Precious few acts are truly selfless, and this is only natural. We all want to see our needs fulfilled. Therefore we encourage you to act on your desires, as that will move you forward towards your goals. As long as you value true human value, we will all gladly sponsor you.

> **_"A proper dose of selfishness, makes room for selfless deeds._"**

Our primary goal for participation in innercircles is not just making it a most fun experience, but also to offer you a place for you to grow. We acknowledge your selfish motives to join, and our common objective is to satisfy them in a way that satisfies ours too.

## Mutualism and synergy

In our research area we study [mutualism](../think/economics/mutualism/){: title="Seeking mutual benefit in cooperation"} in its broadest definition, not restricted by its anarchistic interpretation, and derive best-practices that lead to mutual benefit when people are cooperating. These we'll then apply in all of our work, in the circles we build, and in the community center itself.

> **_Synergy is the creation of a whole that is greater than the simple sum of its parts._**

The objective is not only to apply these practices, but apply them in such way that maximum synergy results from the cooperation. We want this approach to be deeply embedded into our culture, and become part of the mindset of every circle builder. The methodological search for synergy to get anything done, is something we both learn and teach to others.

## Continuous evolution



## Find the triple win

In business we learn to look for win-win situations, i.e. mutual benefits. But oftentimes the opportunity is lost to seek extra synergy on top of that. A triple win is a win-win situation with extra synergy. The <b class="color-think">innercircles</b> <span class="color-feel">triple-win™</span> is an important and powerful people profit tool. It is a means to do:

> **_"Value investment for well-balanced human relations."_**

When all parties in a triple-win are aware of its existence they are referred to as either **friends** or as **partners**, whatever is more appropriate. Until that time they are neutrally indicated as **party** of the triple win. So the triple-win indicates a friendship or a partnership.

**Investopedia:**{: .color-act #value-investment} **Value investment** ➜ Investment paradigm that involves gifting human value that appears undervalued by some form of common sense. [See [old definition](https://en.wikipedia.org/wiki/Value_investing)].
{: .notice--success}

A triple-win can be:

- **informal**: just an understanding between friends, a 'balance sheet' that exists in the head.
- **formal**: a deal or relationship that is recorded in a document or online system.
- **one-sided**: where the other party or parties do not yet know the triple-win exists.
- **unbalanced**: because the generosity and friendship of the participants allows that.

A triple-win is stronger when it is:

- **public**: placing no restriction to who can view its contents.
- **open**: placing no restriction on parties that want to join to add synergy.
- **timeless**: having neither a start date, nor due dates that have to be followed.
- **non-binding**: leading to no commitments or obligations unless willingly engaged.
- **trustraising**: started on a trust-first basis and bestowing ever more trust along the way.

### Formal triple wins

In more business-like settings, where there is no friendship relation, a formal triple-win is the best way to track & trace mutual benefit. A formal triple-win is _always_ tailor-made for the parties involved, but can be derived from a template. Some elements of a formal triple-win can be:

- **balance sheet**: a [ledger](https://en.wikipedia.org/wiki/Bookkeeping#Ledgers) recording beneficial deeds that parties already invested.
- **commitment pacts**: a [promise](#promises) of intended level of future investment on ledger entries.
- **synergy plan**: the [investment strategies](#investment-strategy) of the triple-win or individual ledger entries.
- **visibibility**: level to which others have access to the triple-win or individual ledger entries.
- **accessibility**: level to which the triple-win or individual ledger entries are open.

**Note:**{: .color-feel} Even between friends it can be beneficial to track a formal triple-win to chase synergy more efficiently.
{: .notice--warning}

### One-sided triple wins

We use a lot of works and services that benefit our daily life, where the creators are unaware of our use. Also we may want to support or seek cooperation with parties that are aligned in some way or form to the things we pursue ourself. In that case we can set up a one-sided triple-win, formal or informal. A partnership that the other side is not yet aware of. Why would you do that?

> **_Pay for your appreciation of others with deeds of gratitude, and pay them forward._**

A good example is open source software. You use a great free drawing tool to create your professional images. In open source culture it is common practice that you show your gratitude by helping the sofware project further, by reporting bugs, giving feedback, or a modest donation. And you promote the software to others. You create synergy this way. Appreciation of the maintainer may lead to better support. The next software release will be better because of you. The maintainer benefits too.

It is a true triple-win, and by the way it works there is greater likelyhood that the mainter wants to become a partner of you, should that be your goal.

### Unbalanced triple wins

Allowing the balance of mutual benefits in a triple-win to be favoring the other parties involved is an investment with a higher change of increased mutual benefit in the longer term. It creates synergy. Whereas if you immediately demand there be balance, you are in fact shortselling yourself of this opportunity. However there are two conditions that need to be met:

- **harmonious feelings**: you do not have any bad feelings about the imbalance.
- **harmonious partners**: your relationship to the others is in good spirit and trust.

**Investopedia:**{: .color-act} **Shortselling** ➜ acting in such a way that a person will diminish the amount of mutual benefit to be had. [See [old definition](https://en.wikipedia.org/wiki/Shortselling)]
{: .notice--success}

Note that the unbalance should not raise any expectation of profit within you. If you do expect Return On Investment, then that points to a flaw in yourself, and is an incurrence of debt on the other parties. You unfairly placed a commitment on them, that was never part of the triple-win.

Note also that an 'unbalanced triple win' is just a theoretical construct to help find synergy. As you learn more about the people profit and [flourishing life](../../../about/principles/flourishing-life) philosophies you realize that in a relation of trust between friends, there cannot be an unbalance. There is always enough counterbalance in terms of human value, e.g. love, friendship, forgiveness, gratitue, etc.

**Investopedia:**{: .color-act} **ROI** ➜ **Return Of Involvement**, i.e. creating a connection of inclusion. [See [old definition](https://en.wikipedia.org/wiki/Return_on_investment)]
{: .notice--success}

### The Hero's triple win

This special type of triple win is reserved for projects or initiatives that are so beneficial to mankind and well aligned with our values that we proclaim them to be among our Heroes. We absolutely adore them and will do everything in our power to help them be successful so they can continue to thrive.

An example is our Hero's triple win with **Wikimedia**, and you'll find this in our [**think**](/think/){: .color-think} space:

<div class="notice--think" markdown="1">
  <h3 markdown="1"><span class="fas fa-trophy"></span> [Hero's triple-win:](/feel/mutualism/investment-guide/#the-heros-triple-win){: .color-think} [The Wikimedia Movement](/feel/triple-win/heroes/#-the-wikimedia-movement){: .color-feel}</h3>
  We embrace [**Wikipedia**](https://en.wikipedia.org){: .color-feel}. In all our documentation we will favour Wikipedia in knowledge references. We will improve accuracy of existing pages and create new ones for all our knowledge and insights, where applicable.
</div>

The Hero's triple win is a commitment by us, that never needs anything in return. The mere existence of the project or initiative alone already warrants our full support. These triple wins always start as _one-sided_ and probably will stay that way too. Our contributions to them are our way to show gratitude for what our Hero does.

- [**Honour our heroes**](/feel/triple-win/heroes){: .color-feel}: 

A hero triple win is like the Nobel prize, but in an innercircles context, and clearly only bestowed on rare occasions. Though there are many candidates we'll only consider granting them when our concrete projects and activities have enough overlap with their cause and direction of their work.

### Pacts

A pact in a formal triple-win indicates a common understanding of the level of commitment the parties intend to give. Note that this is an intention only. Without the Pact mentioned, and in informal triple-wins this level is by default a zero-commitment pact.

- **zero commitment**: an understanding without obligations, commitments or expectations.
- **full commitment**: a full embracement on best-effort basis, without expectations.

## Gifting and giftwrapping

The act of giving and a [gift culture](https://en.wikipedia.org/wiki/Gift_economy) play a central role within innercircles. Because a gift well given always enriches all parties involved, end even the world at large will benefit.

### Gifts

**TODO**{: .color-play}
{: .notice--primary}

### Promises

A promise constitutes a gift in innercircles. In life promises are subjective to many human flaws that may lead for them to be broken. This in turn may lead to misunderstanding, misgivings and an erosion of trust. We strive to minimize these effects. Promises are part of our trust-first approach:

> **_"A promise is a gift wrapped with best intention to fulfill the commitment inside."_**

This translates to the following expectations:

- **gifter**: makes a commitment for themself to take a best-effort attempt to fullfil the promise.
- **giftee**: appreciates the token of the promise, and considers its fulfillment an additional gift.

The gifter not following up on a promise can have two reasons:

- **circumstance**: gifter cannot follow up due to causes that are out their control.
- **overcommitment**: too many commitments, or human flaws that stand in the way.

Upon realization of a broken promise the gifter has a _moral incentive_ - not an obligation - to:

- **inform the giftee**: explain the reason circumstance or overcommitment openly.
- **learn from experience**: analyse the reason and find improvements for the future.

**TODO**{: .color-play}
{: .notice--primary}

### Giftwrapping

In Funniest Home Video's you often see clips of small children who are more delighted with the wrapping paper than the gift itself. The paper is more fun to play with. It has more value. At older age they are taught that the gift wrapping is mere trash. As people crave for ever more material wealth they forget what is intrinsically valuable, and that:

> **_The beauty of a gift comes with its packaging._**

In innercircles we strive to bring that value back, and wrap our gifts with great attention, so that its receiver will be as delighted as a child. We practice to become artisans of giftwrapping.

**TODO**{: .color-play}
{: .notice--primary}

## Matchmaking and marriage

**TODO**{: .color-play}
{: .notice--primary}

### Self-disclosure and debiasing

**Investopedia:**{: .color-act} **Intellectual property** ➜ the collective works of intellect, imagination and creativity that humanity creates. [See [old definition](https://en.wikipedia.org/wiki/Intellectual_property)]
{: .notice--success}

**Investopedia:**{: .color-act} **IP rights protection** ➜ safeguarding cultural heritage and freedom - the intelligent progress - of humanity against malign influences. [See [old definition](https://en.wikipedia.org/wiki/Intellectual_property)]
{: .notice--success}

**TODO**{: .color-play} Recognize cognitive biases, increase intellectual property, Wikipedia: [self-disclosure](https://en.wikipedia.org/wiki/Self-disclosure), [debiasing](https://en.wikipedia.org/wiki/Debiasing).
{: .notice--primary}

## Tease, nudge, and play

Teasing, nudging and playing games are three people profit strategies that will be pervasive in all the activities and services that innercircles provides. Therefore it is very important to define accurately what the meaning of these concepts are.

<div class="notice--play" markdown="1">
  <h3><span class="fas fa-theater-masks"></span> teaserbot.. tease, nudge, play!</h3>
  This section only provides a summary. The three strategies are part of the [teaserbot realm](../../../act/ecosystem/#-the-teaserbot-realm) and will be elaborated there.
</div>

### The delicious tease

**TODO**{: .color-play}
{: .notice--primary}

### The gentle nudge

**TODO**{: .color-play}
{: .notice--primary}

### The delightful play

**TODO**{: .color-play}
{: .notice--primary}