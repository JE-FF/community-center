---
permalink: /feel/mutualism/
title: "Mutualism and synergy"
excerpt: "."
sidebar:
  - image: /assets/images/site/sidebar-feel.png
    image_alt: "smallcircles"
    text: 'Radiate our support. Help people help people thrive. <b class="color-feel">Let&#39;s feel!</b>'
  - nav: "nav-feel"
---